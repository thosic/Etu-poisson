-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Dim 04 Octobre 2015 à 13:12
-- Version du serveur: 5.5.24-log
-- Version de PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `etupoiss`
--

-- --------------------------------------------------------

--
-- Structure de la table `compte2`
--

CREATE TABLE IF NOT EXISTS `compte2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` text NOT NULL,
  `nom` text NOT NULL,
  `prenom` text NOT NULL,
  `utilise` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `compte2`
--

INSERT INTO `compte2` (`id`, `login`, `nom`, `prenom`, `utilise`) VALUES
(1, 'sicardth', 'S', 'T', 0);

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

CREATE TABLE IF NOT EXISTS `cours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` int(11) NOT NULL,
  `uv` text NOT NULL,
  `type` text NOT NULL,
  `groupe` int(11) NOT NULL,
  `jour` text NOT NULL,
  `n_jour` int(11) NOT NULL,
  `h_deb` int(11) NOT NULL,
  `m_deb` int(11) NOT NULL,
  `deb` int(11) NOT NULL,
  `h_fin` int(11) NOT NULL,
  `m_fin` int(11) NOT NULL,
  `fin` int(11) NOT NULL,
  `duree_h` int(11) NOT NULL,
  `duree_m` int(11) NOT NULL,
  `frequence` int(11) NOT NULL,
  `semaine` int(11) NOT NULL,
  `salle` int(11) NOT NULL,
  `afficher` int(11) NOT NULL,
  `empreinte` text NOT NULL,
  `confirm` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `cours`
--

INSERT INTO `cours` (`id`, `email`, `uv`, `type`, `groupe`, `jour`, `n_jour`, `h_deb`, `m_deb`, `deb`, `h_fin`, `m_fin`, `fin`, `duree_h`, `duree_m`, `frequence`, `semaine`, `salle`, `afficher`, `empreinte`, `confirm`) VALUES
(1, 0, 'EN21', 'C', 0, 'VENDREDI', 4, 8, 0, 800, 10, 0, 1000, 2, 0, 1, 0, 0, 1, 'EN214800FA2050', 1),
(2, 0, 'EN21', 'D', 4, 'VENDREDI', 4, 10, 15, 1015, 11, 45, 1145, 1, 30, 1, 0, 0, 1, 'EN2141015FA6160', 1),
(3, 0, 'EN21', 'T', 2, 'MERCREDI', 2, 14, 30, 1430, 18, 30, 1830, 4, 0, 2, 1, 0, 1, 'EN2121430RJ2071', 1),
(4, 0, 'TN12', 'C', 0, 'MARDI', 1, 8, 30, 830, 10, 0, 1000, 1, 30, 1, 0, 0, 1, 'TN121830FA2010', 1),
(5, 0, 'TN12', 'D', 5, 'MERCREDI', 2, 8, 0, 800, 12, 0, 1200, 4, 0, 1, 0, 0, 1, 'TN122800FA5120', 1),
(6, 0, 'CT02', 'C', 0, 'JEUDI', 3, 8, 0, 800, 10, 0, 1000, 2, 0, 1, 0, 0, 1, 'CT023800FA2050', 1),
(7, 0, 'CT02', 'D', 1, 'JEUDI', 3, 16, 30, 1630, 18, 30, 1830, 2, 0, 1, 0, 0, 1, 'CT0231630FA3180', 1),
(8, 0, 'MP02', 'C', 0, 'VENDREDI', 4, 16, 30, 1630, 18, 30, 1830, 2, 0, 1, 0, 0, 1, 'MP0241630FA1060', 1),
(9, 0, 'MP02', 'D', 2, 'VENDREDI', 4, 14, 15, 1415, 16, 15, 1615, 2, 0, 1, 0, 0, 1, 'MP0241415FA5090', 1),
(10, 0, 'DD02', 'C', 0, 'LUNDI', 0, 15, 15, 1515, 16, 15, 1615, 1, 0, 1, 0, 0, 1, 'DD0201515FA1060', 1),
(11, 0, 'DD02', 'D', 2, 'LUNDI', 0, 17, 30, 1730, 18, 30, 1830, 1, 0, 1, 0, 0, 1, 'DD0201730FB2090', 1),
(12, 0, 'LA03', 'D', 2, 'MARDI', 1, 10, 15, 1015, 12, 15, 1215, 2, 0, 1, 0, 0, 1, 'LA0311015FC2030', 1),
(13, 0, 'LA03', 'T', 3, 'JEUDI', 3, 14, 15, 1415, 15, 15, 1515, 1, 0, 1, 0, 0, 1, 'LA0331415FC2110', 1);

-- --------------------------------------------------------

--
-- Structure de la table `graphique`
--

CREATE TABLE IF NOT EXISTS `graphique` (
  `id` int(11) NOT NULL,
  `login` text NOT NULL,
  `orientation` int(11) NOT NULL,
  `titre` text NOT NULL,
  `soustitre` text NOT NULL,
  `cadre` text NOT NULL,
  `grille` int(11) NOT NULL,
  `align` text NOT NULL,
  `groupe` int(11) NOT NULL,
  `cases` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `graphique`
--

INSERT INTO `graphique` (`id`, `login`, `orientation`, `titre`, `soustitre`, `cadre`, `grille`, `align`, `groupe`, `cases`) VALUES
(0, 'sicardth', 0, 'Emploi du temps', ' - A2015', '#E6E6E6+0', 0, 'g', 1, '#FF0000+0-#FF8000+1-#FFFF00+0-#80FF00+0-#0080FF+0-#FF0080+0-#FF0000+0-#FF8000+1-#FFFF00+0-#80FF00+0-#0080FF+0-#FF0080+0-');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
