<?php 

/*
Copyright (C) 2011  Thomas SICARD - sicard.th@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *	comparaison_etapes2.php
 *	Comparaison, traite les résultats de comparaison_etape1.php
 *
 */


session_start();
include('bdd.php');

// Compteur

$fichier_compteur = fopen('compteur_comparaison2.txt', 'r+');

$compteur_aff = fgets($fichier_compteur);
$compteur_aff = $compteur_aff + 1;

fseek($fichier_compteur, 0);
fputs($fichier_compteur, $compteur_aff);

fclose($fichier_compteur);

function Prenom_Nom ($mail, $indicateur="p_n"){
	preg_match("#([a-z\-]+)\.([a-z\-]+)@#", $mail, $email);
	
	$prenom = preg_replace("#-#", " ", $email[1]);
	$prenom = ucwords($prenom);
	
	$nom = preg_replace("#-#", " ", $email[2]);
	$nom = ucwords($nom);
	
	if($indicateur == 'p')
		return $prenom;
	if($indicateur == 'n')
		return $nom;
	if($indicateur == 'pn')
		return $prenom.$nom;
	if($indicateur == 'p_n'){
		$retour = mysql_query("SELECT * FROM compte2 WHERE login='". $mail ."'");
		$info_compte = mysql_fetch_array($retour);
		$info_compte['prenom'] = preg_replace('#\s#', '', $info_compte['prenom']);
		$info_compte['nom'] = preg_replace('#\s#', '', $info_compte['nom']);
		return $info_compte['prenom'] . ' ' . $info_compte['nom'];
	}
}


if(isset($_SESSION['membre_id'])){
	
	$tab_empreinte[0][0] = $_SESSION['membre_id'];

	if($_POST['destinataire'] == 'unique'){
		$i = 1;
		$retour_comptes = mysql_query("SELECT * FROM compte2");
		while($info_compte = mysql_fetch_array($retour_comptes)){
			$info_compte['prenom'] = preg_replace('#\s#', '', $info_compte['prenom']);
			$info_compte['nom'] = preg_replace('#\s#', '', $info_compte['nom']);		
			if(isset($_POST[$info_compte['prenom'] . $info_compte['nom']])){
				$tab_empreinte[$i][0] = $info_compte['login'];				
				$i++;
			}
		}
		
		$nb_personnes = $i;
		
		for($j=0 ; $j < $i ; $j++){
			$retour_comptes = mysql_query("SELECT COUNT(*) nb_cours FROM cours WHERE email ='". $tab_empreinte[$j][0] ."'");
			$cours = mysql_fetch_array($retour_comptes);
			$tab_empreinte[$j][1] = $cours['nb_cours'];
		}
		
		for($i = 0; $i < $nb_personnes; $i++){
			$j = 0;
			$retour_cours = mysql_query("SELECT * FROM cours WHERE email='". $tab_empreinte[$i][0] ."' ORDER BY n_jour,h_deb,m_deb");
			while($info_cours = mysql_fetch_array($retour_cours)){
				$tab_empreinte[$i][$j + 2] = $info_cours['empreinte'];
				$j++;
			}
		}
		$nb_cours = $tab_empreinte[0][1];
		
		/// COMPARAISON ///
		
		// Création d'un tableau à double entrée
		for($j = 0; $j < $tab_empreinte[0][1]; $j++){
			$tab_comparaison[0][$j] = $tab_empreinte[0][$j+2];
		}
		for($i = 1; $i <= $nb_personne; $i++){
			for($j = 0; $j < $tab_empreinte[0][1]; $j++){
				$tab_comparaison[$i][$j] = 0;
			}
		}
		
		for($i = 1; $i < $nb_personnes; $i++){
			for($j = 0; $j < $nb_cours; $j++){
				for($k=0; $k < $tab_empreinte[$i][1]; $k++){
					if($tab_empreinte[0][$j+2] == $tab_empreinte[$i][$k+2]){
						$tab_comparaison[$i][$j] = 1;
						$tab_comparaison[$nb_cours][$j] = 1;
					}
				}
			}
		}
		$tab_empreinte[0][0] = $_SESSION['membre_id'];
	}
	
	
	if($_POST['destinataire'] == 'groupe'){
		$tab_empreinte[0][0] = $_SESSION['membre_id'];
		$i = 1;
		$retour_comptes = mysql_query("SELECT * FROM compte2");
		while($info_compte = mysql_fetch_array($retour_comptes)){
			$info_compte['prenom'] = preg_replace('#\s#', '', $info_compte['prenom']);
			$info_compte['nom'] = preg_replace('#\s#', '', $info_compte['nom']);
			if(isset($_POST[$info_compte['prenom'] . $info_compte['nom']])){
				$tab_empreinte[$i][0] = $info_compte['login'];				
				$i++;
			}
		}
		
		$nb_personnes = $i;
		
		for($j=0 ; $j < $i ; $j++){
			$retour_comptes = mysql_query("SELECT COUNT(*) nb_cours FROM cours WHERE email ='". $tab_empreinte[$j][0] ."'");
			$cours = mysql_fetch_array($retour_comptes);
			$tab_empreinte[$j][1] = $cours['nb_cours'];
		}
		
		for($i = 0; $i < $nb_personnes; $i++){
			$j = 0;
			$retour_cours = mysql_query("SELECT * FROM cours WHERE email='". $tab_empreinte[$i][0] ."' ORDER BY n_jour,h_deb,m_deb");
			while($info_cours = mysql_fetch_array($retour_cours)){
				$tab_empreinte[$i][$j + 2] = $info_cours['empreinte'];
				$j++;
			}
		}
		$nb_cours = $tab_empreinte[0][1];
		
		/// COMPARAISON ///
		
		/* Création d'un tableeau à double entrée / Structure :
		
		+-------------++---+---+---+---++---+
		| empreinte 1 || 1 | 1 | 0 | 0 || 2 |
		+-------------++---+---+---+---++---+
		| empreinte 2 || 0 | 1 | 1 | 1 || 3 |
		+-------------++---+---+---+---++---+
		| empreinte 3 || 0 | 1 | 0 | 1 || 2 |
		+-------------++---+---+---+---++---+
		| empreinte 4 || 0 | 0 | 1 | 0 || 1 |
		+-------------++---+---+---+---++---+
		| empreinte 5 || 1 | 0 | 0 | 1 || 2 |
		+-------------++---+---+---+---++---+
		  personnes :    A   B   C   D    
		
		*/
		
		$ligne_act = 0;
		for($i = 0; $i < $nb_personnes; $i++){
			for($j = 0; $j < $tab_empreinte[$i][1]; $j++){
				$temoin = 1;
				for($k =0; $k < $ligne_act; $k++){
					if($tab_empreinte[$i][$j+2] == $liste_empreinte[$k][0]){
						$temoin = 0;
					}
				}
				if($temoin == 1){
					$liste_empreinte[$ligne_act][0] = $tab_empreinte[$i][$j+2];
					for($k = 0; $k <= $nb_personnes; $k++)
						$liste_empreinte[$ligne_act][$k+1];
						
					$ligne_act++;
				}
			}
		}
		$nb_lignes = $ligne_act;
		
		// On remplit le tableau (colonnes correspondant aux personnes A, B, C...
		
		for($i = 0; $i < $nb_personnes; $i++){
			for($j = 0; $j < $nb_lignes; $j++){
				for($k = 0; $k < $tab_empreinte[$i][1]; $k++){
					if($liste_empreinte[$j][0] == $tab_empreinte[$i][$k+2]){
						$liste_empreinte[$j][$i+1] =1;
					}
				}
			}
		}
		
		// On remplit la dernière colonne
		
		for($i = 0; $i < $nb_lignes; $i++){
			for($j = 0; $j < $nb_personnes; $j++){
				$liste_empreinte[$i][$nb_personnes+1] += $liste_empreinte[$i][$j+1];
			}
		}
				
		// Création d'un tableau à double entrée
		/*for($i = O; $i < $nb_cours; $i++){
			$liste_empreinte[$i][0] = $tab_empreinte[0][$i+2];
			for($m = 0; $m < $nb_personnes; $m++)
				$liste_empreinte[$i][$m+1] = 0;
			$nb_cours_liste = $i+1;		
		}
		for($j = 1; $j < $nb_personnes; $j++){
			for($k = 0; $k < $tab_empreinte[$j][1]; $k++){
				$temoin = 1;
				for($l = 0; $l < $nb_cours_liste; $l++){
					if($tab_empreinte[$j][$k+2] == $liste_empreinte[$l])
						$temoin = 0;
				}
				if($temoin == 1){
					$liste_empreinte[$nb_cours_liste] = $tab_empreinte[$j][$k+2];
					for($m = 0; $m < $nb_personnes; $m++)
						$liste_empreinte[$nb_cours_liste][$m+1] = 0;
					$nb_cours_liste++;
				}
			}
		}
		
		for($i = 0; $i < $nb_cours_liste; $i++){
			for($j = 1; $j < $nb_personnes)
			// il faut cocher les cases ou la personne a un cours puis rajouter dans la derniere colonne un 1 SI il y a plus de 2 personnes dans le cours
		}
		
		for($j = 0; $j < $tab_empreinte[0][1]; $j++){
			$tab_comparaison[0][$j] = $tab_empreinte[0][$j+2];
		}
		for($i = 1; $i <= $nb_personne; $i++){
			for($j = 0; $j < $tab_empreinte[0][1]; $j++){
				$tab_comparaison[$i][$j] = 0;
			}
		}
		
		
		for($i = 1; $i < $nb_personnes; $i++){
			for($j = 0; $j < $nb_cours; $j++){
				for($k=0; $k < $tab_empreinte[$i][1]; $k++){
					if($tab_empreinte[0][$j+2] == $tab_empreinte[$i][$k+2]){
						$tab_comparaison[$i][$j] = 1;
						$tab_comparaison[$nb_cours][$j] = 1;
					}
				}
			}
		}
		*/
		
		
		
	
						
						
						
					
	}
}	
	ob_start();
 	include(dirname(__FILE__).'/comparaison_resultat.php');
	$content = ob_get_clean();
	// conversion HTML => PDF
	require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');
	$html2pdf = new HTML2PDF('P','A4','fr', array(5, 5, 5, 0));
//	$html2pdf->setModeDebug();
	$html2pdf->WriteHTML($content, isset($_GET['vuehtml']));
	$html2pdf->Output('test.pdf');	

mysql_close();
?>	