<?php 
session_start();
include('nb_membres.php');
// Compteur

$fichier_compteur = fopen('compteur_accueil.txt', 'r+');

$compteur_aff = fgets($fichier_compteur);
$compteur_aff = $compteur_aff + 1;

fseek($fichier_compteur, 0);
fwrite($fichier_compteur, $compteur_aff);

fclose($fichier_compteur);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
	<head>
		<title>Accueil</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" media="screen" type="text/css" title="Design" href="design.css" />
		<style type="text/css">

			textarea{
				display:block;
			}
			table{
				width:100%
				border-collapse: collapse;
			}
			
			td{
				width:50%;
				
			}
			td a{
				margin-top:10px;
				color: rgb(35,35,35);
			}
			.spe{
				display:block;
			}
			
			.descr{
				padding: 8px;
				text-align:justify;
				width:630px;
				float:right;
				color: rgb(35,35,35);
			}
			.g{
				width: 400px;
			}
			.tcorps .g{
				border-right:1px solid black;
			}
			.tcorps .d{
				border-left:1px solid black;
			}
			.important a{
				color: rgb(42,71,245);
				font-size: 50px;
			}
			.important {
				width : 335px;
				padding-top:150px;
			}
			
			.semi-important a{
				color: rgb(42,71,245);
				display:block;
			}
			
			.nouv_sem{
				background-color: rgb(230,230,230);
				
			}
			
			.marge{
				padding-left: 30px;
			}
					</style>
					
	
	</head>
	<body>
<?php include('banniere.php'); ?>
<!-- <h1><a href="index.php">Générateur d'emplois du temps utc <span class="petit">(en attendant de trouver un autre nom…)</span></a></h1>-->
<div id="corps">
<?php

if(!isset($_SESSION['membre_id'])){
?>


		<div class="descr">Bienvenue !<br/><br/>Ce site te permet d'afficher ton emploi du temps dans un joli PDF automatiquement ou à partir du mail envoyé par l'administration. <br/><br/>Mais ce site te permet également de comparer ton emploi du temps avec les <?php echo Nb_membres(); ?> autres membres. Mais surtout, lorsque tu connaitras tes semaines de TP, que tu auras peut-être échangé des TD sur <a href="http://wwwassos.utc.fr/trocutc/">Troc'UTC</a> et que tu auras des entretiens, tu pourras venir modifier facilement ton emploi du temps en te connectant ! Tu pourras également exporter ton emploi du temps vers un agenda tel que Google Agenda, et ainsi l'afficher facilement sur ton téléphone !</div>
<div class="important" style="text-align: center;"><a href="connexion2.php">Afficher mon emploi <br/>du temps !</a></div>
</div>
<?php	
}
elseif(isset($_SESSION['membre_id'])){
	
	include('bdd.php');
$retour_membres_id = mysql_query("SELECT * FROM compte2 WHERE login='" . $_SESSION['membre_id'] . "'");
$membre_id = mysql_fetch_array($retour_membres_id);
if($membre_id['login'] == ''){
	echo '<meta http-equiv="Refresh" content="0;URL=connexion2.php">';
}
?>	
	<table class="tcorps">
		<tr><td class="g">
			<table>
				<tr><td><a class="spe" href="etape2.php">Modifier les paramètres graphiques de mon emploi du temps</a></td></tr>
				<tr><td><a class="spe" href="modifier.php">Modifier mon emploi du temps</a></td></tr>
				<tr><td><a class="spe" href="comparaison_etape1.php">Comparer mon emploi du temps avec d'autres personnes</a></td></tr>
				<tr><td><a class="spe" href="nomprenom.php">Mettre à jour mes nom et prénom</a></td></tr>
				<tr><td><a class="spe" href="nouveau.php">Copier un nouvel emploi du temps</a></td></tr>
			</table>
		</td>
		<td class="d" >
		
			<table>
								<tr><td><span class="semi-important"><a href="afficher.php">Afficher mon emploi du temps</span></a></td></tr>
				<tr><td><span class="semi-important"><a href="agenda.php">Exporter l'emploi du temps vers Google Agenda / iCalendar</span></a> (<a href="tuto.pdf">Mode d'emploi</a>)</td></tr>
				
		
			</table>
		
		</td></tr>
	</table>
	
	
</div>
<?php	
}
include("pied.php");
?>



	</body>
</html>