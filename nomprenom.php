<?php 
/*
Copyright (C) 2011  Thomas SICARD - sicard.th@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *	modifier2.php
 *	Modification de l'edt dans la base de donnée, à partir des données reçues par le formulaire de modifier.php
 *
 */


session_start();
include('bdd.php');
if(isset($_SESSION['membre_id'])){
	
	if(!isset($_POST['nom'])){
		?>
		
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
			<head>
				<title>Mise à jour du nom et prénom</title>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<link rel="stylesheet" media="screen" type="text/css" title="Design" href="design.css" />
				<style type="text/css">
					textarea{
						display:block;
					}
							</style>
			</head>
			<body>
				<?php include('banniere.php'); ?>
				
				<h2>Mise à jour du nom et prénom</h2>
				<div id="corps">
					<form method="post" action="nomprenom.php">

						<table><tr><td>Prénom :</td><td><input type="intput" name="prenom" id="prenom"/></td></tr>
						<tr><td>Nom :</td><td><input type="input" name="nom" id="nom"/></td></tr></table>
						<input type="submit" value="Me connecter"/>
					</form>
				</div>
				<?php include("pied.php");?>
			</body>
		</html>
		<?php
	}
	elseif(isset($_POST['nom']) && isset($_POST['nom'])){
		if($_POST['nom'] != '' && $_POST['prenom'] != ''){
			$prenom = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['prenom']))));
			$nom = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['nom']))));
			
			$prenom = ucwords(strtolower($prenom));
			$nom = ucwords(strtolower($nom));
			
			mysql_query("UPDATE compte2 SET prenom='" . $prenom . "',nom='" . $nom . "' WHERE login='" . $_SESSION['membre_id'] . "'");
			
			$_SESSION['info'] = 'Prénom et nom mis à jour correctement.';
			echo '<meta http-equiv="Refresh" content="0;URL=index.php">';
			
		}
		else{
			unset($_POST['nom']);
			$_SESSION['erreur'] = 'Prénom ou nom manquant.';
			echo '<meta http-equiv="Refresh" content="0;URL=nomprenom.php">';
		}
	}
}

?>