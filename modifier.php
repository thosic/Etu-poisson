<?php 
/*
Copyright (C) 2011  Thomas SICARD - sicard.th@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *	modifier.php
 *	Page affichant les formulaire de modification de l'edt
 *
 */


session_start();
include('bdd.php');

if(isset($_SESSION['membre_id'])){
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
	<head>
		<title>Étape 2 : paramètres</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" media="screen" type="text/css" title="Design" href="design.css" />
		<style type="text/css">
			textarea{
				display:block;
			}
			table{
				border-collapse: collapse;
			}
			.par_uv td, th{
				border : 1px solid black;
				padding : 2px;
			}
			.par_uv div{
				border: 1px solid black;
			}
			
			.XS{
				width: 30px;
			}

			/*body{
				background-image: url("fond.jpg");
				background-repeat : repeat-x;
				margin-left:0px;
				margin-top:0px;
			}

			h1{
				width:100%;
				background-color:white;
				margin-top:0px;
				padding: 4px;
				font-size: 20px;
				color: rgb(100,100,100);
				border-bottom: 1px solid rgb(100,100,100);
			}
			fieldset{
				border: 2px ridge yellow;
				margin: 4px;
				width: 1000px;
			}
			legend{
				color: rgb(70,70,70);
			}*/

		</style>
	</head>
	<body>

<?php



	
	/***********************************
	RÉCUPÉRATION DES COURS DANS UN ARRAY
	***********************************/
	
	
	$retour_liste_cours = mysql_query("SELECT * FROM cours WHERE email ='". $_SESSION['membre_id'] ."'");

	$i=0;
	while($ligne = mysql_fetch_array($retour_liste_cours)){
		
		//enregistre toutes les données de l'emploi du temps dans un array
		if($detail['uv'] != "SPJE"){
			$edt[$i] = array (
				'uv'		=> $ligne['uv'],
				'type' 		=> $ligne['type'],
				'groupe'	=> $ligne['groupe'],
				'jour'		=> $ligne['jour'],
				'n_jour'	=> $ligne['n_jour'],
				'h_deb'		=> $ligne['h_deb'],
				'm_deb'		=> $ligne['m_deb'],
				'deb'		=> $ligne['deb'],
				'h_fin'		=> $ligne['h_fin'],
				'm_fin'		=> $ligne['m_fin'],
				'fin'		=> $ligne['fin'],
				'duree_h'	=> $ligne['duree_h'],
				'duree_m'	=> $ligne['duree_m'],
				'frequence'	=> $ligne['frequence'],
				'semaine'	=> $ligne['semaine'],
				'salle'		=> $ligne['salle'],
				'afficher'	=> $ligne['afficher'],
				'empreinte'	=> $ligne['empreinte']
			);	
			$i++;
		}
		
	}
	/*****************************
	RÉCUPÉRATION DU TITRE DE L'EDT
	*****************************/
	
	// Titre principal par défaut
	
	$titre_defaut = "Emploi du temps";
	
	// Niveau (TCxx ou GXxx)
	
	//
	// IMPOSSIBLE DE LE RETROUVER POUR L'INSTANT
	//
	
	// Semestre (Axx ou Pxx)
	
	$annee = date('Y');
	
	$mois = date('m');
	if($mois >= 2 && $mois < 8){
		$semestre = 'Printemps ' . $annee;
	}
	else{
		$semestre = 'Automne ' . $annee;
	}
	
	// Prénom
	
	preg_match("#([a-z\-]+)\.([a-z\-]+)@etu\.utc\.fr#", $_SESSION['membre_id'], $email);
	
	$prenom = preg_replace("#-#", " ", $email[1]);
	$prenom = ucwords($prenom);
	
	$nom = preg_replace("#-#", " ", $email[2]);
	$nom = ucwords($nom);

	
	if($prenom != '' || $nom != '')
		$titre_B = $prenom . ' ' . $nom . ' - ';
	if($niveau[0] != '')
		$titre_B = $titre_B . $niveau[0] . ' - ';
	
	$titre_B = $titre_B . $semestre;

	// E-mail
	
	$email = $email[0];
	
	/***************
	LISTE DES DES UV
	***************/
	
	$i = 0;
	$liste_uv = array ();
	foreach($edt as $ligne){
		$tampon = 1;
		foreach($liste_uv as $uv_courante){
			if($uv_courante == $ligne['uv'])
				$tampon = 0;
		}
		if($tampon == 1){
			$liste_uv[$i] = $ligne['uv'];
			$i++;
		}	
	}
	
	

	/************************************
	AFFICHAGE DU MENU DE PERSONNALISATION
	************************************/
	

	
	
	$str_edt = serialize($edt);
	//echo $str_edt;
	$str_edt = preg_replace("#\"#", "/", $str_edt);
	//echo $str_edt;
	
	$str_uv = serialize($liste_uv);
	//echo $str_edt;
	$str_uv = preg_replace("#\"#", "/", $str_uv);
	//echo $str_edt;
	include('banniere.php'); 
	echo '<h2>Modifier mon emploi du temps (étape 1 : modifier ou supprimer des cours)</h2><div id="corps">';

	
	echo '<form action="modifier2.php" method="post">';
	
	///// CARRE PARAMETRES GENERAUX /////
	
	echo '<table class="par_uv">';
	
	$i = 0;
	$j = 0;
	foreach($liste_uv as $ligne){
		//echo '<fieldset><legend>Paramètres pour ' . $ligne .'</legend>';
		echo '<tr><td>' . $ligne . '</td><td style="padding: 8px;">';

		$j++;
		
		echo '<table style="margin-top: 8px;">';
		echo '<tr><th>cours, TD ou TP</th><th>jour</th><th>heure de début</th><th>durée</th><th>semaine</th><th>salle</th><th>Afficher</th><th>Supprimer</th></tr>';
		
		foreach($edt as $ligneb){
			if($ligneb['uv'] == $ligne){
				echo '<tr>';
				
				// Type
				echo '<td><select name="'. $i .'_type">';
					echo '<option value="C" ';
						if($ligneb['type'] == 'C'){ echo 'selected="selected"'; }
					echo '>Cours</option>';
					
					echo '<option value="D" ';
						if($ligneb['type'] == 'D'){ echo 'selected="selected"'; }
					echo '>TD</option>';
					
					echo '<option value="T" ';
						if($ligneb['type'] == 'T'){ echo 'selected="selected"'; }
					echo '>TP</option>';
					
					echo '<option value="E" ';
						if($ligneb['type'] == 'E'){ echo 'selected="selected"'; }
					echo '>Entretien</option>';
					
				echo '</select></td>';
				
				// Jour
				echo '<td><select name="'. $i .'_jour">';
					echo '<option value="LUNDI" ';
						if($ligneb['jour'] == 'LUNDI'){ echo 'selected="selected"'; }
					echo '>Lundi</option>';
					
					
					echo '<option value="MARDI" ';
						if($ligneb['jour'] == 'MARDI'){ echo 'selected="selected"'; }
					echo '>Mardi</option>';	
					
					
					echo '<option value="MERCREDI" ';
						if($ligneb['jour'] == 'MERCREDI'){ echo 'selected="selected"'; }
					echo '>Mercredi</option>';
					
					
					echo '<option value="JEUDI" ';
						if($ligneb['jour'] == 'JEUDI'){ echo 'selected="selected"'; }
					echo '>Jeudi</option>';
					
					
					echo '<option value="VENDREDI" ';
						if($ligneb['jour'] == 'VENDREDI'){ echo 'selected="selected"'; }
					echo '>Vendredi</option>';
		
		
					echo '<option value="SAMEDI" ';
						if($ligneb['jour'] == 'SAMEDI'){ echo 'selected="selected"'; }
					echo '>Samedi</option>';
				echo '</select></td>';
		
				// heure de début
				echo '<td><input class="XS" type="text" name="'. $i .'_h_deb" value="'. $ligneb['h_deb'] .'"/>h<input class="XS" type="text" name="'. $i .'_m_deb" value="'. $ligneb['m_deb'] .'"/></td>';
				
				// durée
				echo '<td><input class="XS" type="text" name="'. $i .'_duree_h" value="'. $ligneb['duree_h'] .'"/>h<input class="XS" type="text" name="'. $i .'_duree_m" value="'. $ligneb['duree_m'] .'"/></td>';
				
				// fréquence
				echo '<td><select name="'. $i .'_frequence">';
					echo '<option value="0" ';
					if($ligneb['semaine'] == 0){ echo 'selected="selected"'; }
					echo '>Toutes les semaines</option>';
					
					echo '<option value="1" ';
					if($ligneb['semaine'] == 1){ echo 'selected="selected"'; }
					echo '>Semaines A</option>';
					
					echo '<option value="2" ';
					if($ligneb['semaine'] == 2){ echo 'selected="selected"'; }
					echo '>Semaines B</option>';
				echo '</select></td>';
				
				// salle
				echo '<td><input type="text" name="'. $i .'_salle" value="'. $ligneb['salle'] .'"/></td>';
				
				// affichage
				echo '<td><input type="checkbox" name="'. $i .'_afficher" ';
					if($ligneb['afficher'] == 1){ echo 'checked="checked"';}
				echo '></td>';
				
				// Suppression
				echo '<td><input type="checkbox" name="'. $i .'_supprimer"></td>';
				echo '</tr>';
				
				echo '<input type="hidden" name="'. $i .'_empreinte" value="'. $ligneb['empreinte'] .'">';

				
				$i++;
			}
			
		}
		echo '</table>';
		echo '</td></tr>';
		
	}
	
	echo '</table>';
	
	/// POUR AJOUTER DES COURS ///
	
	echo '<h2>Modifier mon emploi du temps (étape 2 : ajouter de nouveaux cours)</h2><div id="corps">';
	echo '<div class="corps">';

	
		
		
		
		
		// Ajouter des évènements
		

	

	
			for($i = 0 ; $i <20; $i++){
			
				$texte_ajout = '<div id="table_'. $i .'" style="display:none;">Cours ou activité supplémentaire n°'. ($i+1) . ' :';
				$texte_ajout .= '<table class="par_uv" style="margin-bottom:16px;">';
				$texte_ajout .='<tr>';
				$texte_ajout .=	'<th>UV ou intitulé</th>';
				$texte_ajout .=	'<th>Type</th>';
				$texte_ajout .=	'<th>jour</th>';
				$texte_ajout .=	'<th>heure de début</th>';
				$texte_ajout .=	'<th>durée</th>';
				$texte_ajout .=	'<th>semaine</th>';
				$texte_ajout .=	'<th>salle</th>';
				$texte_ajout .='</tr>';
			
			
				$texte_ajout .=  '<tr>';
						// UV
				$texte_ajout .=	'<td><input type="text" name="ajouter_'. $i .'_uv"/></td>';
						// Type
				$texte_ajout .=	 '<td><select name="ajouter_'. $i .'_type">';
				$texte_ajout .=				 '<option value="C">Cours</option>';
				$texte_ajout .=				 '<option value="D">TD</option>';
				$texte_ajout .=				 '<option value="T">TP</option>';
				$texte_ajout .=				 '<option value="E">Entretien</option>';
				$texte_ajout .=				 '<option value="A">Autre</option>';
				$texte_ajout .=			 '</select></td>';
						// Jour
				$texte_ajout .=			 '<td><select name="ajouter_'. $i .'_jour">';
				$texte_ajout .=				 '<option value="LUNDI">Lundi</option>';
				$texte_ajout .=				 '<option value="MARDI">Mardi</option>';
				$texte_ajout .=				 '<option value="MERCREDI">Mercredi</option>';
				$texte_ajout .=				 '<option value="JEUDI">Jeudi</option>';
				$texte_ajout .=				 '<option value="VENDREDI">Vendredi</option>';
				$texte_ajout .=				 '<option value="SEMEDI">Samedi</option>';
				$texte_ajout .=			 '</select></td>';
						// Début				
				$texte_ajout .=			 '<td><input type="text" name="ajouter_'. $i .'_h_deb" class="XS"/>h<input type="text" name="ajouter_'. $i .'_m_deb" class="XS"/></td>';
						// Durée
				$texte_ajout .=			 '<td><input type="text" name="ajouter_'. $i .'_duree_h" class="XS"/>h<input type="text" name="ajouter_'. $i .'_duree_m" class="XS"/></td>';
						// Semaine
				$texte_ajout .=			 '<td><select name="ajouter_'. $i .'_semaine">';
				$texte_ajout .=				 '<option value="0">Toutes les semaines</option>';
				$texte_ajout .=				 '<option value="1">Semaines A</option>';
				$texte_ajout .=				 '<option value="2">Semaine B</option>';
				$texte_ajout .=			 '</select></td>';
						// Salle
				$texte_ajout .=			 '<td><input type="text" name="ajouter_'. $i .'_salle"/></td>';
				$texte_ajout .=		 '</tr></table></div>';
				
				echo $texte_ajout;
			}
			
			
?>
			<script type="text/javascript">
				document.getElementById('table_0').style.display="block";
		
			
			

			
						
		var i = 0;
		function ajouterCours(){
			i++;
			document.getElementById('table_'+ i).style.display="block";
			//sdocument.getElementById('ajout').innerHTML .= ;
		}
			</script>
				
			
	
	
		<div id="bouton_ajout" onclick="ajouterCours()" style="font-size:10pt;text-decoration:underline;margin-top:10px">Ajouter un cours / une activité supplémentaire</div>







<?php



		/*for($i=0; $i < 3; $i++){
			echo '<tr>';
				// UV
				echo '<td><input type="text" name="ajouter_'. $i .'_uv"/></td>';
				// Type
				echo '<td><select name="ajouter_'. $i .'_type">';
					echo '<option value="C">Cours</option>';
					echo '<option value="D">TD</option>';
					echo '<option value="T">TP</option>';
					echo '<option value="E">Entretien</option>';
					echo '<option value="A">Autre</option>';
				echo '</select></td>';
				// Jour
				echo '<td><select name="ajouter_'. $i .'_jour">';
					echo '<option value="LUNDI">Lundi</option>';
					echo '<option value="MARDI">Mardi</option>';
					echo '<option value="MERCREDI">Mercredi</option>';
					echo '<option value="JEUDI">Jeudi</option>';
					echo '<option value="VENDREDI">Vendredi</option>';
					echo '<option value="SEMEDI">Samedi</option>';
				echo '</select></td>';
				// Début				
				echo '<td><input type="text" name="ajouter_'. $i .'_h_deb" class="XS"/>h<input type="text" name="ajouter_'. $i .'_m_deb" class="XS"/></td>';
				// Durée
				echo '<td><input type="text" name="ajouter_'. $i .'_duree_h" class="XS"/>h<input type="text" name="ajouter_'. $i .'_duree_m" class="XS"/></td>';
				// Semaine
				echo '<td><select name="ajouter_'. $i .'_semaine">';
					echo '<option value="0">Toutes les semaines</option>';
					echo '<option value="1">Semaines A</option>';
					echo '<option value="2">Semaine B</option>';
				echo '</select></td>';
				// Salle
				echo '<td><input type="text" name="ajouter_'. $i .'_salle"/></td>';
			echo '</tr>';
		}
		
	echo '</table>';	*/
	
	
	/////////////////////////////////
	
/*	
	echo '<fieldset><legend>Paramètres des cours, TD et TP</legend>';
	
	echo '<table>';
	echo '<tr><th>UV</th><th>cours, TD ou TP</th><th>jour</th><th>heure de début</th><th>durée</th><th>semaine</th><th>salle</th><th>Afficher</th></tr>';
	
	
	// Personnalisation des cours
	$i = 0;
	foreach($edt as $ligne){
		
		echo '<tr>';
		
		// UV
		echo '<td>'. $ligne['uv'] .'</td>';
		
		// Type
		echo '<td><select name="'. $i .'_type">';
			echo '<option value="C" ';
				if($ligne['type'] == 'C'){ echo 'selected="selected"'; }
			echo '>Cours</option>';
			
			echo '<option value="D" ';
				if($ligne['type'] == 'D'){ echo 'selected="selected"'; }
			echo '>TD</option>';
			
			echo '<option value="T" ';
				if($ligne['type'] == 'T'){ echo 'selected="selected"'; }
			echo '>TP</option>';
			
			echo '<option value="E" ';
				if($ligne['type'] == 'E'){ echo 'selected="selected"'; }
			echo '>Entretien</option>';
			
		echo '</select></td>';
		
		// Jour
		echo '<td><select name="'. $i .'_jour">';
			echo '<option value="LUNDI" ';
				if($ligne['jour'] == 'LUNDI'){ echo 'selected="selected"'; }
			echo '>Lundi</option>';
			
			
			echo '<option value="MARDI" ';
				if($ligne['jour'] == 'MARDI'){ echo 'selected="selected"'; }
			echo '>Mardi</option>';	
			
			
			echo '<option value="MERCREDI" ';
				if($ligne['jour'] == 'MERCREDI'){ echo 'selected="selected"'; }
			echo '>Mercredi</option>';
			
			
			echo '<option value="JEUDI" ';
				if($ligne['jour'] == 'JEUDI'){ echo 'selected="selected"'; }
			echo '>Jeudi</option>';
			
			
			echo '<option value="VENDREDI" ';
				if($ligne['jour'] == 'VENDREDI'){ echo 'selected="selected"'; }
			echo '>Vendredi</option>';


			echo '<option value="SAMEDI" ';
				if($ligne['jour'] == 'SAMEDI'){ echo 'selected="selected"'; }
			echo '>Samedi</option>';
		echo '</select></td>';

		// heure de début
		echo '<td><input class="XS" type="text" name="'. $i .'_h_deb" value="'. $ligne['h_deb'] .'"/>h<input class="XS" type="text" name="'. $i .'_m_deb" value="'. $ligne['m_deb'] .'"/></td>';
		
		// durée
		echo '<td><input class="XS" type="text" name="'. $i .'_duree_h" value="'. $ligne['duree_h'] .'"/>h<input class="XS" type="text" name="'. $i .'_duree_m" value="'. $ligne['duree_m'] .'"/></td>';
		
		// fréquence
		echo '<td><select name="'. $i .'_frequence">';
			echo '<option value="0" ';
			if($ligne['semaine'] == 0){ echo 'selected="selected"'; }
			echo '>Toutes les semaines</option>';
			
			echo '<option value="1" ';
			if($ligne['semaine'] == 1){ echo 'selected="selected"'; }
			echo '>Semaines A</option>';
			
			echo '<option value="2" ';
			if($ligne['semaine'] == 2){ echo 'selected="selected"'; }
			echo '>Semaines B</option>';
		echo '</select></td>';
		
		// salle
		echo '<td><input type="text" name="'. $i .'_salle" value="'. $ligne['salle'] .'"/></td>';
		
		// affichage
		echo '<td><input type="checkbox" name="'. $i .'_afficher" checked="checked"></td>';
		echo '</tr>';
		
		$i++;
	}
	
	echo '</table>';
	echo '</fieldset>';
	*/
		echo '<input type="hidden" name="edt" value="'. $str_edt .'">';
		echo '<input type="hidden" name="uv" value="'. $str_uv .'">';
		echo '<input type="hidden" name="nom" value="'. $nom .'">';
		echo '<input type="hidden" name="prenom" value="'. $prenom .'">';
		echo '<br/><input type="submit" value="Modifier / Supprimer / Ajouter" />';
	echo '</form>';	


?>


	
</div></div></div>
<?php	

include("pied.php");
?>		
	</div>
	</body>
</html>



<?php	
}
?>