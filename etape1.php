<?php 
session_start();
include('bdd.php');

// Compteur

$fichier_compteur = fopen('compteur_affichage1.txt', 'r+');

$compteur_aff = fgets($fichier_compteur);
$compteur_aff = $compteur_aff + 1;

fseek($fichier_compteur, 0);
fputs($fichier_compteur, $compteur_aff);

fclose($fichier_compteur);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
	<head>
		<title>Étape 1 : copie du mail</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" media="screen" type="text/css" title="Design" href="design.css" />
		<style type="text/css">
			textarea{
				display:block;
			}
					</style>
	</head>
	<body>

<?php include('banniere.php'); ?>	
<h2>Étape 1 : copie du mail</h2>

<div id="corps">

<form method="post" action="etape2.php">
	<label for="mail">Copier le mail reçu pour l'emploi du temps :</label>
	<textarea name="mail" id="mail" cols="128" rows="32"></textarea>
	<input type="submit" value="Continuer (étape 2)" />
</form>

</div>
<?php include("pied.php");?>

	</body>
</html>
