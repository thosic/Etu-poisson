<?php

/*
Copyright (C) 2011  Thomas SICARD - sicard.th@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *	pied.php
 *	Contient les pieds de pages
 *
 */




include('bdd.php');

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
	<head>
		<title>Connexion</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" media="screen" type="text/css" title="Design" href="design_banniere.css" />
		<style type="text/css">
			html{
				padding : 0px;
				margin : 0px;
			}
			body{
				padding : 0px;
				margin : 0px;
			}
			textarea{
				display:block;
			}
			.legende{
				position : absolute;
				top: 50px;
				left: 0px;
				color: white;
				font-size : 24px;
				width: 992px;
				padding : 4px;
				//border: 1px solid blue;
				margin: 0px;
				
			}
		
			.pied{
			
				
				
				width : 988px;
				padding: 6px;
				background-color : rgb(31,31,31);
				margin:none;
				background-image: url("pied.png");
				background-repeat : repeat-x;
				color: white;
			}
			.pied a {
				color:white;
			}
			
			img{
				padding: 0px;
				 margin : none;
				 display: block;
			}
			
			a img{
				border: none;
			}
			table{
				width: 100%;
				border-collapse: collapse;
			}
			.gp,.dp{
				width: 50%;
				padding: 8px;’
			}
			.tpied .gp{
				border-right:1px solid white;
			}
			.tpied .dp{
				border-left:1px solid white;
				text-align: center;
			}
		</style>
	</head>
	<body>
	

<div style="clear:both;"></div>
<div class="pied">
<table class="tpied">
		<tr><td class="gp">Site réalisé par Thomas Sicard.<br/> Merci à Aurélien Dumaine d'avoir réalisé le générateur de fichier ics. Merci à Nicolas Cellier pour l'hébergement qu'il me fournit. Merci à Nicolas Puech d'avoir fait la publicité pour le site. Merci à <a href="http://wwwetu.utc.fr/~paussnic/emploi_du_temps/">Nicolas Pauss</a> pour le code qu'il m'a fournit (choix des couleurs). Merci à tous ceux qui m'ont rapporté les différents bugs et à ceux qui m'ont donné des idées d'améliorations pour le site.<br/>Enfin merci à tous les utilisateurs, grâce auxquels tous les efforts de développement du site ne sont pas vains !</td>
		<td class="dp" >Pour me contacter envoie moi un mail à : <br/><span>tho</span>mas.<span>sic<span>ard arob<span>ase e</span>tu.ut<span>c.fr</span></td></tr>
	</table>
</div>
		
		
		
	</body>
</html>

<?php
mysql_close();
?>