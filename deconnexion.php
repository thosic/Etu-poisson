<?php


//Paramètres du serveur CAS
define('SERVEUR_SSO', 'cas.utc.fr');
define('PORT_SSO', 443);
define('RACINE_SSO', 'cas');

//Inialisation du serveur CAS
    include_once('CAS/CAS/CAS.php');
    phpCAS::client(CAS_VERSION_2_0,SERVEUR_SSO,PORT_SSO,RACINE_SSO);
    phpCAS::setLang('french');
    phpCAS::setNoCasServerValidation();


//bloc de connexion
function co(){
	phpCAS::forceAuthentication();

	//on stocke le login dans la variale de session correspondante
	$_SESSION['membre_id'] = phpCAS::getUser();
}


//bloc pour la deconnexion
function deco(){
	session_destroy();
	phpCAS::logout();
}


deco();



?>