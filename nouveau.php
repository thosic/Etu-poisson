<?php /*
Copyright (C) 2011  Thomas SICARD - sicard.th@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *	nouveau.php
 *	Page permettant de copier 'emploi du temps en début de semestre pour les personnes déjà inscrites
 *
 */


session_start();
include('bdd.php');
if(isset($_SESSION['membre_id'])){
	
	function semestre(){
		// Enregistrer le semestre dans la liste des comptes
		$annee = date('Y');
	
		$mois = date('m');
		if($mois < 2){
			$semestre = 'A' . ($annee - 1);
		}
		elseif($mois >= 2 && $mois < 8){
			$semestre = 'P' . $annee;
		}
		elseif($mois >= 8){
			$semestre = 'A' . $annee;
		}
		return $semestre;
	}

	
	if($_POST['mail'] == '' && isset($_POST['mail'])){
		$_SESSION['erreur'] = 'L\'emploi du temps est vide';
	}
	
	if(!isset($_POST['mail']) || $_POST['mail'] == ''){
		
		
		// Mettre des paramètre graphiques par défaut :
		$retour_graph = mysql_query("SELECT * FROM graphique WHERE login='" . $_SESSION['membre_id'] . "'");
		$graph = mysql_fetch_array($retour_graph);
		
		// si on trouve ça dans la BDD UTC :
		if($graph["login"] == ''){
			$email = $_SESSION['membre_id'];
			$sstitre = $niveau . ' - ' . semestre();
			
			$query = "INSERT INTO graphique VALUES('', '". $email ."', '0', 'Emploi du temps', '". $sstitre ."', '#E6E6E6+0', '0', 'g', '1', '#FF0000+0-#FF8000+1-#FFFF00+0-#80FF00+0-#0080FF+0-#FF0080+0-#FF0000+0-#FF8000+1-#FFFF00+0-#80FF00+0-#0080FF+0-#FF0080+0-')";
				mysql_query($query);
		}
		
		
		
		

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
	<head>
		<title>Étape 1 : copie du mail</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" media="screen" type="text/css" title="Design" href="design.css" />
		<style type="text/css">
			textarea{
				display:block;
			}
					</style>
	</head>
	<body>

<?php include('banniere.php'); ?>

<h2>Enregistrer mon nouvel emploi du temps</h2>

<div id="corps">

<form method="post" action="nouveau.php">
	<label for="mail">Copier le mail reçu pour l'emploi du temps :</label>
	<textarea name="mail" id="mail" cols="128" rows="32"></textarea>
Attention, l'ancien emploi du temps sera effacé !
<br/>
	<input type="submit" value="Enregistrer" />
</form>

</div>
<?php include("pied.php");?>

	</body>
</html>

<?php
	}
	
	if(isset($_POST['mail']) && $_POST['mail'] != ''){
		// DECHIFFRAGE
		
		$mail= mysql_real_escape_string(htmlspecialchars($_POST['mail']));
		
		//enregistrement dans un premier array
		$mail = preg_replace("#\.\.\.\\\\r\\\\n#", '...', $mail);
		
		$mail = preg_replace("#([A-Z0-9]{4}(\s)+[CDT](\s*[0-9])*(\s)+)((LUNDI|MARDI|MERCREDI|JEUDI|VENDREDI|SAMEDI|DIMANCHE)\.*(\s)+[0-9]{1,2}:[0-9]{1,2}-(\s)*[0-9]{1,2}:[0-9]{1,2},F[0-9],S=[A-Z0-9\s]+(\s)+)/(((LUNDI|MARDI|MERCREDI|JEUDI|VENDREDI|SAMEDI|DIMANCHE)\.*(\s)+[0-9]{1,2}:[0-9]{1,2}-(\s)*[0-9]{1,2}:[0-9]{1,2},F[0-9],S=[A-Z0-9\s]+))+#", '$1$5<br/>$1$10', $mail);
		echo $mail;
		$format = "#[A-Z0-9]{4}(\s)+[CDT](\s*[0-9])*(\s)+(LUNDI|MARDI|MERCREDI|JEUDI|VENDREDI|SAMEDI|DIMANCHE)\.*(\s)+[0-9]{1,2}:[0-9]{1,2}-(\s)*[0-9]{1,2}:[0-9]{1,2},F[0-9],S=[A-Z0-9\s]+#";
		preg_match_all($format, $mail, $heures_tamp);
		
		// scindage de l'array en "sous-array"
		$i=0;
		foreach($heures_tamp[0] as $ligne){
			
			// uv
			preg_match("#[A-Z0-9]{4}#", $ligne, $case);
			$detail['uv'] = $case[0];
			
			// type (cours, TD ou TP)
			preg_match("#\s[CDT](\s*[0-9])*#", $ligne, $case);
			preg_match("#[CDT]#", $case[0], $case);
			$detail['type'] = $case[0];
			
			// groupe du TD ou TP
			preg_match("#\s[CDT](\s*[0-9])*#", $ligne, $case);
			preg_match("#[0-9]+#", $case[0], $case);
			$detail['groupe'] = $case[0];
			if($detail['groupe'] == '')
				$detail['groupe'] = 0;
			
			// jour du cours
			preg_match("#(LUNDI|MARDI|MERCREDI|JEUDI|VENDREDI|SAMEDI|DIMANCHE)#", $ligne, $case);
			$detail['jour'] = $case[0];
			
			if($detail['jour'] == 'LUNDI')
				$detail['n_jour'] = 0;
			elseif($detail['jour'] == 'MARDI')
				$detail['n_jour'] = 1;
			elseif($detail['jour'] == 'MERCREDI')
				$detail['n_jour'] = 2;
			elseif($detail['jour'] == 'JEUDI')
				$detail['n_jour'] = 3;
			elseif($detail['jour'] == 'VENDREDI')
				$detail['n_jour'] = 4;
			elseif($detail['jour'] == 'SAMEDI')
				$detail['n_jour'] = 5;
			
			// heures de début et de fin
			preg_match("#([0-9]{1,2}):([0-9]{1,2})-[\s]*([0-9]{1,2}):([0-9]{1,2})#", $ligne, $case);
			$detail['h_deb'] = $case[1];
			$detail['m_deb'] = $case[2];
			$detail['h_fin'] = $case[3];
			$detail['m_fin'] = $case[4];
			
			// durée
			$duree_h = $detail['h_fin'] - $detail['h_deb']; 
			$duree_m = $detail['m_fin'] - $detail['m_deb'];
			if($duree_m < 0){
				$duree_h--;
				$duree_m = $duree_m + 60;
			}
			if($duree_m == 0)
				$duree_m = '00';
			
			// fréquence : toutes les semaines ou deux
			preg_match("#F[0-9]#", $ligne, $case);
			preg_match("#[0-9]#", $case[0], $case);
			$detail['frequence'] = $case[0];
			
			if($detail['frequence'] != 1)
				$detail['semaine'] = 1;
			elseif($detail['frequence'] == 1)
				$detail['semaine'] = 0;
			
			// salle
			preg_match("#=[A-Z0-9]+#", $ligne, $case);
			preg_match("#[A-Z0-9]+#", $case[0], $case);
			$detail['salle'] = $case[0];
			
			
			
			
			//Exceptions
			if($detail['salle'] == 'XXXXX')
				$detail['salle'] = '0';
			
			
			//enregistre toutes les données de l'emploi du temps dans un array
			if($detail['uv'] != "SPJE"){
				$edt[$i] = array (
					'uv'		=> $detail['uv'],
					'type' 		=> $detail['type'],
					'groupe'	=> $detail['groupe'],
					'jour'		=> $detail['jour'],
					'n_jour'	=> $detail['n_jour'],
					'h_deb'		=> $detail['h_deb'],
					'm_deb'		=> $detail['m_deb'],
					'deb'		=> $detail['h_deb']*100+$detail['m_deb'],
					'h_fin'		=> $detail['h_fin'],
					'm_fin'		=> $detail['m_fin'],
					'fin'		=> $detail['h_fin']*100+$detail['m_fin'],
					'duree_h'	=> $duree_h,
					'duree_m'	=> $duree_m,
					'frequence'	=> $detail['frequence'],
					'semaine'	=> $detail['semaine'],
					'salle'		=> $detail['salle']
				);
				$i++;
			}
			
		}
		// ENREGISTREMENT
		
		$nb_cours = $i;
		$query1 = "DELETE FROM cours WHERE email='". $_SESSION['membre_id'] ."'";
		mysql_query($query1);
		for($i = 0; $i < $nb_cours; $i++ ){

			// Création de l'empreinte
			$empreinte = $edt[$i]['uv'];
			$empreinte .= $edt[$i]['n_jour'];
			$empreinte .= $edt[$i]['deb'];
			$empreinte .= $edt[$i]['salle'];
			$empreinte .= $edt[$i]['semaine'];
			
			
			
			$query2 = "INSERT INTO cours VALUES('', '". $_SESSION['membre_id'] ."', '". $edt[$i]['uv'] ."', '". $edt[$i]['type'] ."','". $edt[$i]['groupe'] ."','". $edt[$i]['jour'] ."','". $edt[$i]['n_jour'] ."','". $edt[$i]['h_deb'] ."','". $edt[$i]['m_deb'] ."','". $edt[$i]['deb'] ."','". $edt[$i]['h_fin'] ."','". $edt[$i]['m_fin'] ."','". $edt[$i]['fin'] ."','". $edt[$i]['duree_h'] ."','". $edt[$i]['duree_m'] ."','". $edt[$i]['frequence'] ."','". $edt[$i]['semaine'] ."','". $edt[$i]['salle'] ."','1'  ,'". $empreinte ."', '1')";
			
			mysql_query($query2);
			echo '+';
		}	
			// Enregistrer le semestre dans la liste des comptes
			$annee = date('Y');
	
			$mois = date('m');
			if($mois < 2){
				$semestre = 'A' . ($annee - 1);
			}
			elseif($mois >= 2 && $mois < 8){
				$semestre = 'P' . $annee;
			}
			elseif($mois >= 8){
				$semestre = 'A' . $annee;
			}
			

						
			$semestre = semestre();
			$query3 = "UPDATE compte2 SET utilise='". $semestre ."' WHERE login='". $_SESSION['membre_id'] ."'";
			mysql_query($query3);
			
		
		
		$_SESSION['info'] = "Le nouvel emploi du temps a bien été enregisté, tu peux maintenant l'afficher et le modifier.";
		echo '<meta http-equiv="Refresh" content="0;URL=index.php">';
	}
}
?>