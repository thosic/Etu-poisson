		<style type="text/css">
			
			.ligne_horaires{
				
				vertical-align: top;
				text-align: center;
				font-weight: bold;
				background-color: rgb(<?php echo $col_cadre['r'] . ',' . $col_cadre['g'] . ',' . $col_cadre['b']; ?>);
				color: <?php echo $col_cadre['txt'] ; ?>;
			}
			
			.ligne_horaires_haut{
				
				border-top: 1px solid rgb(128,128,128);
				height: 20px;
				vertical-align: middle;
				text-align: left;
				font-weight: bold;
				background-color: rgb(<?php echo $col_cadre['r'] . ',' . $col_cadre['g'] . ',' . $col_cadre['b']; ?>);
				color: <?php echo $col_cadre['txt'] ; ?>;
				
			}
			
			.ligne_horaires_bas{
				
				border-bottom: 1px solid rgb(128,128,128);
				height: 20px;
				vertical-align: middle;
				text-align: left;
				font-weight: bold;
				background-color: rgb(<?php echo $col_cadre['r'] . ',' . $col_cadre['g'] . ',' . $col_cadre['b']; ?>);
				color: <?php echo $col_cadre['txt'] ; ?>;
				
			}
			
			
			.corps_BCDZ{
				border-right: 1px solid rgb(128,128,128);
				border-bottom: 1px solid rgb(128,128,128);
			}
			
			.ligne_reperes{
				height: 0px;
				background-color: rgb(<?php echo $col_cadre['r'] . ',' . $col_cadre['g'] . ',' . $col_cadre['b']; ?>);
				color: <?php echo $col_cadre['txt'] ; ?>;
				
			}
			
			.colonne_jour{
				text-align: center;
				font-weight: bold;
				background-color: rgb(<?php echo $col_cadre['r'] . ',' . $col_cadre['g'] . ',' . $col_cadre['b']; ?>);
				color: <?php echo $col_cadre['txt'] ; ?>;
			}
			
			
			
			
						
			table{
				margin: 0px;
				border-collapse: collapse;
				<?php if($a5 ==1){
					echo 'font-size : 11px;';;
				}
				else
					echo 'font-size : 14px;';
				?>
				
			}
			
			em{
				font-style: italic;
				<?php if($a5 ==1)
					echo 'font-size : 9px;';
				else
					echo 'font-size : 14px;';
				?>
			}
			
			strong{
				font-weight : bold;
				<?php if($a5 ==1){
					echo 'font-size : 11px;';;
				}
				else
					echo 'font-size : 14px;';
				?>
			}
			
			.titre_A{
				position: relative;
				top: 0mm;
				width : 100%;
				height: 10mm;
				<?php if($a5 ==1)
					echo 'text-align: left;';
				else
					echo 'text-align: center;';
				?>
				font-size: 10mm;
				//overflow: hidden;
				font-family: Helvetica, Arial, serif;
				padding: 0px;
				margin-top: 0px;
				margin: 0px;
				margin-bottom: 1mm;
				
				
			}
			
			.titre_B{
				position : absolute;
				right:0px;
				top: 3mm;
				height: 4mm;
				//overflow: hidden;
				font-family: Helvetica, Arial, serif;
				font-size: 4mm;
				color: rgb(128,128,128);
				padding: 0px;
				margin: 0px;
			}
			
			
			
			

			
			.big td{
				padding: 0px;
			}
			
			
			
			/*Colonnes d'horaires*/
			

			
			
			.col_horaires_caseXg, .col_horaires_case0g, .col_horaires_caseXmg{
				height: 26px;
				padding: 1px;
				vertical-align: top;
				text-align: center;
				font-weight: bold;
				background-color: rgb(<?php echo $col_cadre['r'] . ',' . $col_cadre['g'] . ',' . $col_cadre['b']; ?>);
				border-left: 1px solid rgb(128,128,128);
				color: <?php echo $col_cadre['txt'] ; ?>;
			}
			
			.col_horaires_caseXd, .col_horaires_case0d, .col_horaires_caseXmd{
				width: 26px;
				padding: 1px;
				vertical-align: top;
				text-align: center;
				font-weight: bold;
				background-color: rgb(<?php echo $col_cadre['r'] . ',' . $col_cadre['g'] . ',' . $col_cadre['b']; ?>);
				border-right: 1x solid rgb(128,128,128);
				color: <?php echo $col_cadre['txt'] ; ?>;
			}
			
			.col_horaires_case0g, .col_horaires_case0d{
				width: <?php echo $l_colonneA; ?>;
				
			}
			
			.col_horaires_caseXmg, .col_horaires_caseXmd{
				border-bottom: 1px solid rgb(128,128,128);
			}
			
			
			/*colonnes inter*/
			
			.col_inter_case0{
				width : 1mm;
				height : 23px;
				border-top: 1px solid rgb(128,128,128);
				border-bottom: 1px solid rgb(128,128,128);
				background-color: rgb(<?php echo $col_cadre['r'] . ',' . $col_cadre['g'] . ',' . $col_cadre['b']; ?>);
			}
			
			.col_inter_caseX{
				width : 1mm;
				border-top: 1px solid rgb(128,128,128);
				border-bottom: 1px solid rgb(128,128,128);
				background-color: rgb(<?php echo $col_cadre['r'] . ',' . $col_cadre['g'] . ',' . $col_cadre['b']; ?>);
			}
			
			/*tableau principal*/
						
			.grille th{
				height: 23px;
				overflow: hidden;
				border: 1px solid rgb(128,128,128);
				background-color: rgb(<?php echo $col_cadre['r'] . ',' . $col_cadre['g'] . ',' . $col_cadre['b']; ?>);
				text-align: center;
				color: <?php echo $col_cadre['txt'] ; ?>;
			}
			
			.grille .caseX{
				border-bottom: 1px solid rgb(128,128,128);
				border-left: 1px solid rgb(128,128,128);
				border-right: 1px solid rgb(128,128,128);
				vertical-align: top;
				padding: 0px;
			}
			
			
			
			
			.demi-grille{
				border-bottom : 1px solid rgb(192,192,192);
				font-size : 1px;
				padding:0px;
			}
			
			
			/*cases cours*/
			
			.case_cours{
				background-color : white;
				border : 1px solid black;
				padding: 2px;
				overflow: hidden;
			}
			
			/*légende*/
			
			.legende{
				margin-top:2mm;
				color: rgb(128,128,128);
				vertical-align:top;
				font-size:12px;
			}
			
		</style>



<?php
	include('bdd.php');
		
	if($a5 == 1)
		echo '<page orientation="portrait" style="font-size: 12pt">';
	else
		echo '<page orientation="paysage" style="font-size: 12pt" backtop="0mm" backbottom="-5mm" backleft="0mm" backright="0mm">';
	
	/****
	TITRE
	****/
	
	echo '<div class="titre_A">'. $titre_A .'</div>';
	echo '<div class="titre_B">'. $titre_B .'</div>';

	/*****
	GRILLE
	*****/
	
	// LIGNES HORAIRES (fonction)
	function creerLigneHoraires($cote, $nb_heures, $heure_deb, $l_caseA, $l_caseBCD){
		
		echo '<table class="ligne_horaires"><tr>';
		
		$style = 'width:' . ($l_caseA - 2*(1 + 2) - 10) . 'px;'; // - 2 * ( border + padding )
		$style .= 'border-left: 1px solid rgb(128,128,128);';
		$style .= 'padding: 1px;';
		
		echo '<td class="ligne_horaires_'. $cote .'" style="'. $style .';"></td>';
		
		
		$style = 'width:' . ($l_caseBCD - 2*2) . 'px;';
		$style .= 'padding: 1px;';
		
		for($i=0; $i < $nb_heures - 1; $i++)			
			echo '<td class="ligne_horaires_'. $cote .'" style="' . $style . '">'. ($heure_deb + $i) .'h</td>';
			
			
			
		$l_caseZ = $l_caseBCD - 2*(1 + 2) + 10;
		$style = 'width:' . $l_case7 . 'px;';
			
		$style = 'width:' . $l_caseZ . 'px;'; // - border - padding
		$style .= 'border-right: 1px solid rgb(128,128,128);';
		$style .= 'padding: 1px;';
			
		echo '<td class="ligne_horaires_'. $cote .'" style="'. $style .'">'. ($heure_deb + $i) .'h</td>';
		

		echo '</tr></table>';
	}
	
	function creerLigneReperes($nb_heures, $l_caseA, $l_caseBCD){
		echo '<table>';
		
			
			echo '<tr>';
			
			// Colonne du jour
			
			$style = 'width:' . ($l_caseA - 2 * (2 + 4)) . 'px;'; // - 2 * ( border + padding )
			$style .= 'border-left: 1px solid rgb(128,128,128);';
			$style .= 'border-right: 1px solid rgb(128,128,128);';
			$style .= 'padding-left: 2px;';
			$style .= 'padding-right: 2px;';
			$style .= 'padding-top: 0px;';
			$style .= 'padding-bottom: 0px;';
		
			echo '<td class="ligne_reperes" style="'. $style .';"></td>';
			
			
			$style = 'width:' . ($l_caseBCD - 2 * (4 + 1)) . 'px;'; // - 2 * ( border + padding )
			$style .= 'padding: 2px;';
			$style .= 'border-right : 1px solid rgb(128,128,128)';
			
			for($j = 0; $j < $nb_heures; $j++){
				echo '<td class="ligne_reperes" style="'. $style .'"></td>';
			}
			echo '</tr>';
		
		echo '</table>';
	}
	
	
	
	echo '<table class="big"><tr><td>'; // tableau BIG
		
		$l_caseBCD = ( $l_cadre - $l_caseA ) / $nb_heures;
		
		
		// Ligne des horaires - haute
		creerLigneHoraires('haut', $nb_heures, $min, $l_caseA, $l_caseBCD);
		creerLigneReperes($nb_heures, $l_caseA, $l_caseBCD);
		
		// Corps de la grille
		$jours = array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
		$h_caseABCDZ = ($h_cadre - 2 * (20 - 2 * (2 + 1)) - 2 * 4 ) / $nbre_jours;
		
		echo '<table>';		
		for($i =0; $i < $nbre_jours; $i++){
			
			echo '<tr>';
			
			// Colonne du jour
			
			$style = 'width:' . ($l_caseA - 2 * (2 + 4)) . 'px;'; // - 2 * ( border + padding )
			$style .= 'height:' . $h_caseABCDZ . 'px;';
			$style .= 'border: 1px solid rgb(128,128,128);';
			$style .= 'padding: 2px;';
			if($i == 0)
				$style .= 'height: '. ($h_caseABCDZ +1.1) .'px;';
		
			echo '<td class="colonne_jour" style="'. $style .';">'. $jours[$i] .'</td>';
			
			
			$style = 'width:' . ($l_caseBCD - 2 * (0 + 1)) . 'px;'; // - 2 * ( border + padding )
			$style .= 'padding: 0px;';
			if($i == 0)
				$style .= 'border-top : 1px solid rgb(128,128,128)';
				
			for($j = 0; $j < $nb_heures; $j++){
				echo '<td class="corps_BCDZ" style="'. $style .'">';
				
					// Grille des minutes
					if($grille == 30){
						echo '<table style="margin: 0px;"><tr style="margin: 0px;">';
						$style_fond = 'padding: 0px;';
						$style_fond .= 'width: ' . ($l_caseBCD - 2 * (0.8 + 1)) / 2 . 'px;';
						$style_fond .= 'height: '. ($h_caseABCDZ + 9) .'px;';
						$style_fond .= 'margin: 0px;';
						$style_fond .= 'border-right: 1px solid rgb(192,192,192);';
						echo '<td style="'. $style_fond .'"></td>';
						echo '</tr></table>';
					}
					if($grille == 15){
						echo '<table style="margin: 0px;"><tr style="margin: 0px;">';
						$style_fond = 'padding: 0px;';
						$style_fond .= 'width: ' . ($l_caseBCD - 2 * (2.5 + 1)) / 4 . 'px;';
						$style_fond .= 'height: '. ($h_caseABCDZ + 9) .'px;';
						$style_fond .= 'margin: 0px;';
						$style_fond .= 'border-right: 1px solid rgb(192,192,192);';
						echo '<td style="'. $style_fond .'"></td>';
						echo '<td style="'. $style_fond .'"></td>';
						echo '<td style="'. $style_fond .'"></td>';
						echo '</tr></table>';
					}
				
				echo '</td>';
			}
			echo '</tr>';
		}
		echo '</table>';
		
		// Ligne des horaires - basse
		creerLigneReperes($nb_heures, $l_caseA, $l_caseBCD);
		creerLigneHoraires('bas', $nb_heures, $min, $l_caseA, $l_caseBCD);
		

		
		
		
		

		
	echo '</td>'; // tableau BIG
	
	/*	creerColonneInter($nb_heures, $h_caseX);
	
	echo '</td><td>'; // tableau BIG
	
		echo '<table class="grille">';
			
			// Ligne des jours
			echo '<tr><th>Lundi</th><th>Mardi</th><th>Mercredi</th><th>Jeudi</th><th>Vendredi</th>';
			if($nbre_jours == 6)
				echo '<th>Samedi</th>';
			echo '</tr>';
			
			// Reste du tableau
			for($i=0; $i < $nb_heures; $i++){
				echo '<tr>';
				for($j = 0; $j < $nbre_jours; $j++){
					echo '<td class="caseX" style="width:'. ($l_caseX + 4) .'px;height:'. ($h_caseX + 4) .'px;">';
					if($grille==30){
						echo '<table><tr><td style="width:' . ($l_caseX + 6) .'px; height:'. (($h_caseX+2)/2) .'px" class="demi-grille"></td></tr></table>';
					}
					if($grille==15){
						echo '<table><tr><td style="width:'. ($l_caseX + 6) .'px; height:'. ($h_caseX/4-1) .'px" class="demi-grille"></td></tr><tr><td style="width:'. ($l_caseX + 6) .'px; height:'. ($h_caseX/4) .'px" class="demi-grille"></td></tr><tr><td style="width:'. ($l_caseX + 6) .'px; height:'. ($h_caseX/4) .'px" class="demi-grille"></td></tr></table>';
					}
					echo '</td>';
				}
				echo '</tr>';
			}
			
		echo '</table>';
	
	echo '</td><td>'; // tableau BIG
	
			creerColonneInter($nb_heures, $h_caseX);
	
	echo '</td><td>'; // tableau BIG

		creerColonneHoraires('droite', $nb_heures, $min, $h_caseX);
		*/
	echo '</tr></table>'; // tableau BIG
	
	
	/**********
	CASES COURS
	**********/
	$i =0;
	foreach($edt as $ligne){
		
		if($_POST[$i . '_afficher'] == 'on' || $ligne['afficher'] == 1){

			$duree_heure = $ligne['duree_h'];
			$duree_minute = $ligne['duree_m'];
			
			$largeur = $l_caseBCD * ($duree_heure + $duree_minute / 60.5) - 5;
			
			$largeur = 'width=' . $largeur . 'px; ';
			

			
			$hauteur = ($h_caseABCDZ + 7);
			if($ligne['semaine'] != 0)
				$hauteur = $hauteur / 2 -2.5;
			$hauteurb = $hauteur;
			$hauteur = 'height:' . $hauteur . 'px; ' . 'overflow: hidden;';
			
			
			$decalage_bas = 74.9 + ($ligne['n_jour'])*($h_caseABCDZ + 12);
			
			if($ligne['semaine'] == 2)
				$decalage_bas = $decalage_bas + $hauteurb + $ligne['n_jour']*1.7;
			$decalage_bas = 'position : absolute; top:' . $decalage_bas . 'px; ';
			
			
			
			
			
			$decalage_droite = 79 + ($ligne['h_deb'] - $min) * ($l_caseBCD + 0) + ($ligne['m_deb']/60)*($l_caseBCD + 1);
			
			//($ligne['h_deb'] - $min)*($l_caseBCD + 6.07) + ($ligne['m_deb']/60)*($l_caseBCD + 6);
			$decalage_droite = 'left:' . $decalage_droite . 'px; ';
			
			//couleur
			$uv = $ligne['uv']; //récupération de l'UV en cours
			$r = $liste_uv[$uv]['r'];
			$g = $liste_uv[$uv]['g'];
			$b = $liste_uv[$uv]['b'];
			
			$couleur = 'background-color: rgb('. $r .','. $g .',' . $b .'); color:' . $liste_uv[$uv]['txt'] . ';';
			
			$chaine_style = $largeur . $hauteur . $decalage_droite . $decalage_bas . $couleur;
			
			if($ligne['type'] == 'C')
				$ligne['type'] = 'Cours';
			if($ligne['type'] == 'T')
				$ligne['type'] = 'TP';
			if($ligne['type'] == 'D')
				$ligne['type'] = 'TD';
			if($ligne['type'] == 'E')
				$ligne['type'] = 'E';
				
			if($ligne['m_deb'] == 0)
				$ligne['m_deb'] = '00';
			if($ligne['m_fin'] == 0)
				$ligne['m_fin'] = '00';
				
			$ligne['uv'] = preg_replace("#_#", " ", $ligne['uv']);
			$ligne['m_deb'] = preg_replace("#^([0-9]{1})$#", "0$1", $ligne['m_deb']);
			$ligne['m_fin'] = preg_replace("#^([0-9]{1})$#", "0$1", $ligne['m_fin']);
			
			echo '<div class="case_cours" style="'. $chaine_style .'"><strong>'. $ligne['uv'] . '<br/></strong>';
			if($ligne['salle'] != '0')
				echo '<em>' . $ligne['type'] . ' en ' . $ligne['salle'] .'<br/>';
			else
				echo '<em>';
			echo $ligne['h_deb'] . 'h' . $ligne['m_deb'] . ' - ' . $ligne['h_fin'] . 'h' . $ligne['m_fin'] . '</em></div>';
	
		}
		$i++;
	}
	
	
	
	/******
	LEGENDE
	******/
	if($a5 != 1){
		//Compte uvs
		$nb_uv = 0;
		$i = 0;
		foreach($liste_uv as $ligne){
			if(preg_match("#[A-Z0-9]{4}#", $ligne['uv'])){
				$nb_uv++;
				$nom_uv[$i] = $ligne['uv'];
				$i++;
			}
		}
		
		//nombre de colonnes (duos de colonnes) et leur largeur
		
		$nb_col = floor(($nb_uv - 0.1) / 3) + 1;
		$petite_larg = 9; // en mm
		$grande_larg = 297/$nb_col - 8; // en mm
		
		//contenu de la légende
		
		include('bdd.php');
		echo '<table class="legende"><tr>';
		
		$uv_traitees_A = 0;
		$uv_traitees_B = 0;
		for($j = 0; $j < $nb_col; $j++){
			echo '<td style="width: '. $petite_larg .'mm;">';
			
			for($i = 0; $i < 3; $i ++){
				
				$retour_uv = mysql_query("SELECT * FROM uv WHERE uv='" . $nom_uv[3 * $j + $i] . "'");
				$tableau_legende = mysql_fetch_array($retour_uv);
				echo $tableau_legende['uv'] . '<br/>';
				
				$uv_traitees_A++;
				
				if($uv_traitees_A == $nb_uv)
					$i = 3;
			}
			
			echo '</td><td style="width: '. $grande_larg .'mm;">';
			
			for($i = 0; $i < 3; $i ++){
				
				$retour_uv = mysql_query("SELECT * FROM uv WHERE uv='" . $nom_uv[3 * $j + $i] . "'");
				$tableau_legende = mysql_fetch_array($retour_uv);
				echo utf8_encode($tableau_legende['legende']) . '<br/>';
				
				$uv_traitees_B++;
				
				if($uv_traitees_B == $nb_uv)
					$i = 3;
			}
			
			echo '</td>';
		}
		
		echo '</tr></table>';
		
		mysql_close();
	}
	if($a5 == 1)
		echo '<div style="border-bottom:1px dashed rgb(128,128,128); position:absolute;top:-5mm;left:-5mm; width:210mm; height:148mm;"></div>';
	?>
	</page>
