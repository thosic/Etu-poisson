<?php 
/*
Copyright (C) 2011  Thomas SICARD - sicard.th@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *	comparaison_etape1.php
 *	Comparaison, affiche le formulaire contenant la liste des personnes inscrites
 *
 */


session_start();
include('bdd.php');
$alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
$alphamin = 'abcdefghijklmnopqrstuvwxyz';

// Compteur

$fichier_compteur = fopen('compteur_comparaison1.txt', 'r+');

$compteur_aff = fgets($fichier_compteur);
$compteur_aff = $compteur_aff + 1;

fseek($fichier_compteur, 0);
fputs($fichier_compteur, $compteur_aff);

fclose($fichier_compteur);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
	<head>
		<title>Comparaison d'emplois du temps - Etape 1</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" media="screen" type="text/css" title="Design" href="design.css" />
		<style type="text/css">
			td{
				vertical-align:top;
			}
			.lettrine{
				margin-left:40px;
				font-weight: bold;
				font-size : 16pt;
			}
		</style>
	</head>
	<body>

<?php include('banniere.php'); ?>

<h2>Comparer mon emploi du temps (étape 1/2 : choix du type de comparaison)</h2>
<div id="corps">
<?php
if(isset($_SESSION['membre_id'])){
?>
	<form method="post" action="comparaison_etape2.php">
	
		<input type="radio" name="destinataire" value="unique" id="unique" checked="checked"/> <label for="unique">Comparer mon emploi du temps avec celui des autres</label><br/>
		<input type="radio" name="destinataire" value="groupe" id="groupe"/> <label for="groupe">Comparer tous les emplois du temps entre eux</label><br/><br/>
		
</div>
<h2>Comparer mon emploi du temps (étape 2/2 : choix des personnes à comparer)</h2>
<div id="corps">
		Choisis les personnes avec lesquelles tu souhaites comparer ton emploi du temps :<br/><br/>
<?php
		// Création des colonnes...
		
		$retour_comptes = mysql_query("SELECT COUNT(*) nb_comptes FROM compte2");
		$comptes = mysql_fetch_array($retour_comptes);
		$nb_comptes = $comptes["nb_comptes"];
		$nb_colonne = ceil($nb_comptes / 4);
		
		echo '<table><tr>';
			echo '<td>';
		
		// Détérminer le semestre actuel
			$annee = date('Y');
	
			$mois = date('m');
			if($mois < 2){
				$semestre = 'A' . ($annee - 1);
			}
			elseif($mois >= 2 && $mois < 8){
				$semestre = 'P' . $annee;
			}
			elseif($mois >= 8){
				$semestre = 'A' . $annee;
			}
		
		$retour_comptes = mysql_query("SELECT * FROM compte2 WHERE utilise='". $semestre ."' ORDER BY prenom, nom ");
		
		$i = 0;
		$j = -1; // lettre actuelle
		$lettre_actuelle = $alpha[$j];
		while($info_compte = mysql_fetch_array($retour_comptes)){
			if($i == $nb_colonne || $i == $nb_colonne * 2 || $i == $nb_colonne * 3)
				echo '</td><td>';
				
			if( $info_compte['login'] != $_SESSION['membre_id']){
				$info_compte['prenom'] = preg_replace('#\s#', '', $info_compte['prenom']);
				
				// CHANGER DE LETTRE ?
				if($info_compte['prenom'][0] != $lettre_actuelle && $info_compte['prenom'][0] != $alphamin[$j]){
					$j++;
					$boucle = 1;
					while($boucle ==1){
						if($info_compte['prenom'][0] != $alpha[$j])
							$j++;
						else
							$boucle = 0;
					}
					$lettre_actuelle = $alpha[$j];
					echo '<span class="lettrine">' . $lettre_actuelle . '</span><br/>';
				}
				
				$info_compte['nom'] = preg_replace('#\s#', '', $info_compte['nom']);
				echo '<input type="checkbox" name="' . $info_compte['prenom'] . $info_compte['nom'] . '" id="'. $info_compte['login'] .'"/><label for="' .$info_compte['login']. '">' . $info_compte['prenom'] . ' ' . $info_compte['nom'] . '</label><br/>';
			}
			$i++;
		}
		
			echo '</td>';
		echo '</tr></table>'
?>
		<br/><input type="submit" value="Afficher la comparaison"/>
	</form>
<?php
}
mysql_close();
?>	
	</div>
	
<?php	

include("pied.php");
?>
	</body>
</html>

