<?php 

/*
Copyright (C) 2011  Thomas SICARD - sicard.th@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *	banniere.php
 *	Contient l'entête des pages du site : bannière + barre de connexion
 *
 */




include('bdd.php');

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
	<head>
		<title>Connexion</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" media="screen" type="text/css" title="Design" href="design_banniere.css" />
		<style type="text/css">
			html{
				padding : 0px;
				margin : 0px;
			}
			body{
				padding : 0px;
				margin : 0px;
			}
			textarea{
				display:block;
			}
			.legende{
				position : absolute;
				top: 50px;
				left: 0px;
				color: rgb(255,201,72);
				font-size : 24px;
				width: 992px;
				padding : 4px;
				text-align: right;
				//border: 1px solid blue;
				margin: 0px;
				
			}
			.barre{
				width : 988px;
				padding: 6px;
				background-color : blue;
				margin:none;
				height: 29px;
				background-image: url("barre.png");
				background-repeat : repeat-x;
				color: white;
				font-size: 18px;
				text-align: right;
			}
			.barre a {
				color: white;
			}
			
			img{
				padding: 0px;
				 margin : none;
				 display: block;
			}
			
			a img{
				border: none;
			}
		</style>
	</head>
	<body>
	
<a href="index.php"><img src="fondn.png" alt="fond étoilé" style="float:left;"/>
</a>
<div class="barre">
<?php
if(!isset($_SESSION['membre_id'])){
?>
	<a href="connexion2.php">Connexion</a></div>

<?php
}
elseif(isset($_SESSION['membre_id'])){

	echo 'Connecté comme ' . $_SESSION['membre_id'] . ' ';
?>	
	(<a href="deconnexion.php">Déconnexion</a>)</div><div style="clear:both;"></div>
	

<?php
}
?>

<?php

if(isset($_SESSION['info']) && $_SESSION['info'] != '0'){
	echo '<div style="clear:both;"></div><div class="info">';
	echo $_SESSION['info'];
	echo '</div>';
	$_SESSION['info'] = 0;
}
if(isset($_SESSION['erreur']) && $_SESSION['erreur'] != '0'){
	echo '<div style="clear:both;"></div><div class="erreur">';
	echo $_SESSION['erreur'];
	echo '</div>';
	$_SESSION['erreur'] = 0;
}
?>

		
		
		
	</body>
</html>

<?php

?>