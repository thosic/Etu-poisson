<?php 
session_start();
include('bdd.php');
include('nb_membres.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
	<head>
		<title>Étape 2 : paramètres</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" media="screen" type="text/css" title="Design" href="design.css" />
		<script type="text/javascript" src="./moo/mootools.v1.11.js"></script>
		<script type="text/javascript" src="./moo/mooRainbow.js"></script>
		<script type="text/javascript" >
						var tabMooRainbow=new Array();
			function addNewMooRainbow(index, rDefaut, gDefaut, bDefaut){
				tabMooRainbow[index]=new MooRainbow('myRainbow'+index, {
					id: 'input'+index,
					'startColor': [rDefaut, gDefaut, bDefaut],
					onChange: function(color) {
						
						$('color'+index).value = color.hex;
						colorChanging(index,color.hex);
					}
				});
			
			}
			
			function deleteMooRainbow(index){
				tabMooRainbow[index].destruct();
				delete tabMooRainbow[index];
			}
			
			var nbOfUv=<?=count($c)?>;
			
			function ajoutEnt(){
				var choix=document.getElementById("choixEnt").value;
				var textInputEnt=document.getElementById(choix);
				
				var texte="";
				
				texte=document.getElementById("jourEnt").value;
				
				texte+="-";
				texte+=document.getElementById("debEnt").value;
				texte+="-";
				texte+=document.getElementById("finEnt").value;
				texte+="-";
				texte+=document.getElementById("salleEnt").value;
				texte+="-";
				texte+=document.getElementById("semEnt").value;
				texte+=";";
				
				textInputEnt.value+=texte;
				textInputEnt.style.visibility="visible";
				
				return false;
			}
			
			
			var tabActi=new Array();
			function ajoutActi(){
				
				var texte="";
				
				texte=document.getElementById("jourActi").value;
				
				texte+="-";
				texte+=document.getElementById("debActi").value;
				texte+="-";
				texte+=document.getElementById("finActi").value;
				texte+="-";
				texte+=document.getElementById("salleActi").value;
				texte+="-";
				texte+=document.getElementById("semActi").value;
				texte+=";";
				
				
				var nbOfActi=tabActi.length;
				var nbTot=nbOfUv+nbOfActi;
				var nom=document.getElementById("nomActi").value;
				var actiLigne = document.getElementById("acti_"+nom);
				var levelActi=1;
				if(actiLigne){
					if(actiLigne.style.display!="none"){
						document.getElementById("actiHor"+nom).value+=texte;
						levelActi=0;
					}else{
						actiLigne.style.display="table-row";
						levelActi=2;
						nbTot--;
						deleteMooRainbow(nbTot);
					}
				}else{
					actiLigne= document.createElement("tr");
					actiLigne.setAttribute("id","acti_"+nom);
					document.getElementById("mainTable").appendChild(actiLigne);
				}
				if(levelActi>0){
					<?php if($isIE){ ?>
					
					while(actiLigne.cells.length!=0){
						actiLigne.deleteCell(-1);
					}
					var tmpCell=actiLigne.insertCell();
					tmpCell.innerHTML='<input type="hidden" name="acti'+nbOfActi+'" value="'+nom+'" />'+nom+' :';
					tmpCell=actiLigne.insertCell();
					tmpCell.innerHTML='<img id="myRainbow'+nbTot+'" class="color_wheel" alt="[r]" src="color_wheel.gif" /> '+
						'<input type="hidden" name="backgroundColor'+nbTot+'" id="inputColor'+nbTot+'" value="#FFFFFF" /> '+
						'<span class="background" id="bgColor'+nbTot+'" >&#160;</span> '+
						'<input id="myInput'+nbTot+'" type="text" size="8" name="myInput'+nbTot+'" onChange="colorChanging('+nbTot+',this.value);" /> ';
					tmpCell=actiLigne.insertCell();
					tmpCell.innerHTML='<select name="textColor'+nbTot+'">'+
						'<option selected="selected" value="-1">Automatique</option>'+
						'<option value="0">Noir</option>'+
						'<option value="255">Blanc</option>'+
					'</select>';
					tmpCell=actiLigne.insertCell();
					tmpCell.setAttribute("colspan","2");
					tmpCell.innerHTML='<input style="width:100%;" type="text" name="actiHor'+nbOfActi+'" id="actiHor'+nom+'" value="'+texte+'" />';
					
					<?php }else{ ?>
					var texteInnerHTML='<td><input type="hidden" name="acti'+nbOfActi+'" value="'+nom+'" />'+nom+' :</td>'+
					'<td>'+
						'<img id="myRainbow'+nbTot+'" class="color_wheel" alt="[r]" src="color_wheel.gif" /> '+
						'<input type="hidden" name="backgroundColor'+nbTot+'" id="inputColor'+nbTot+'" value="#FFFFFF" /> '+
						'<span class="background" id="bgColor'+nbTot+'" >&#160;</span> '+
						'<input id="myInput'+nbTot+'" type="text" size="8" name="myInput'+nbTot+'" onChange="colorChanging('+nbTot+',this.value);" /> '+
					'</td>'+
					'<td>'+
					'<select name="textColor'+nbTot+'">'+
						'<option selected="selected" value="-1">Automatique</option>'+
						'<option value="0">Noir</option>'+
						'<option value="255">Blanc</option>'+
					'</select>'+
					'</td>'+
					'<td colspan="2">'+
						'<input style="width:100%;" type="text" name="actiHor'+nbOfActi+'" id="actiHor'+nom+'" value="'+texte+'" />'+
					'</td>';
					actiLigne.innerHTML=texteInnerHTML;
					<?php } ?>
					//*/
				}
				addNewMooRainbow(nbTot);	
				if(levelActi==1){
					tabActi.push(actiLigne);
				}
				document.getElementById("nbOfActi").value=tabActi.length;
			
				return false;
			}
			
			
			function resetForm(theForm){
				theForm.reset();
				var i;
				for(i=0;i<tabActi.length;i++){
					acti=tabActi[i];
					acti.style.display="none";
					<?php if($isIE){ ?>
					var tmpCell=acti.insertCell();
					tmpCell.innerHTML='<input type="hidden" name="actiHorSkipped'+i+'" value="skipped" />';
					<?php }else{ ?>
					acti.innerHTML+='<input type="hidden" name="actiHorSkipped'+i+'" value="skipped" />';
					<?php } ?>
				}
				for(i=0;i<nbOfUv;i++){
                                        colorChanging(i,'#FFFFFF');
					document.getElementById("ent"+i).style.visibility="hidden";
				}
				return false;
			}
			
			
			function colorChanging(colorIndex,colorValue){
			
				colorInput=document.getElementById('inputColor'+colorIndex);
				bgColor=document.getElementById('bgColor'+colorIndex);
				
				colorValue=colorValue.toLowerCase();
				colorValue=trim(colorValue);
				if(colorValue.match(/#[A-Fa-f0-9]{6}/)){
					bgColor.style.backgroundColor=colorValue;
					colorInput.value=colorValue;
				}else if(colorValue in nameToHex){
					bgColor.style.backgroundColor=nameToHex[colorValue];
					colorInput.value=nameToHex[colorValue];
				}else{
					bgColor.style.backgroundColor='#FFFFFF';
					colorInput.value='#FFFFFF';
				}
			}
			
			function trim(myString)
			{
				return myString.replace(/^\s+/g,'').replace(/\s+$/g,'')
			} 
		</script>
		
		
			<link type="text/css" href="./moo/mooRainbow_2.css" rel="stylesheet" />
		<style type="text/css">
			textarea{
				display:block;
			}
			table{
				border-collapse: collapse;
			}
			.par_uv td, th{
				border : 1px solid black;
				padding : 2px;
			}
			
			
			.XS{
				width: 30px;
			}
			
			
			.orange{
				color : red;
			}
			
			
			
			
			
			.color_wheel{
				display:inline-block;
				vertical-align:middle;
				height:16px;
			
			}
		
			.ent{
				visibility: hidden;
			
			}
			.background{
				display:inline;
				padding-right:100px;
				margin-right:5px;
				overflow:hidden;
				width:30px;
				height:25px;
				background-color:#FF00FF;
				border:1px black solid;
				vertical-align:middle;
			}
			<?php
			for($i = 0; $i < 10; $i++){
				echo '#span_' . $i . '{display:none;}';
				echo '#spanCache_' . $i . '{display:none;}';
			}
			?>
			/*body{
				background-image: url("fond.jpg");
				background-repeat : repeat-x;
				margin-left:0px;
				margin-top:0px;
			}

			h1{
				width:100%;
				background-color:white;
				margin-top:0px;
				padding: 4px;
				font-size: 20px;
				color: rgb(100,100,100);
				border-bottom: 1px solid rgb(100,100,100);
			}
			fieldset{
				border: 2px ridge yellow;
				margin: 4px;
				width: 1000px;
			}
			legend{
				color: rgb(70,70,70);
			}*/

		</style>
	</head>
	<body>
		<noscript>
			<h1>Veuillez activer le JavaScript puis actualiser la page pour pouvoir utiliser ce script.</h1>
		</noscript>
<?php

function hex_rgb($color){
			$rgb[0]	= hexdec($color[1] . $color[2]);
			$rgb[1]	= hexdec($color[3] . $color[4]);
			$rgb[2]	= hexdec($color[5] . $color[6]);
			return $rgb;
			
		}

if((isset($_POST['mail']) && $_POST['mail'] != '') || isset($_SESSION['membre_id'])){

	/*****************
	PALETTE PAR DEFAUT
	*****************/
	
	$palette[0][0] = '#F7BCC0'; //rouge
	$palette[0][1] = '#FEEBCD'; //orange
	$palette[0][2] = '#FEFFCB'; // jaune
	$palette[0][3] = '#DCFFCB'; // vert I
	$palette[0][4] = '#D2EAFF'; // bleu I
	$palette[0][5] = '#DACDFF'; // violet
	$palette[0][6] = '#F1C6DE'; // rose
	$palette[0][7] = '#EDFFC8'; // vert II
	$palette[0][8] = '#D0D8FF'; // bleu II
	$palette[0][9] = '#D1FFFF'; // bleu III
	
	$palette[1][0] = '#FF0000'; //rouge
	$palette[1][1] = '#FF8000'; //orange
	$palette[1][2] = '#FFFF00'; // jaune
	$palette[1][3] = '#80FF00'; // vert I
	$palette[1][4] = '#0080FF'; // bleu I
	$palette[1][5] = '#FF0080'; // mauve
	$palette[1][6] = '#8000FF'; // violet
	$palette[1][7] = '#00FF80'; // vert II
	$palette[1][8] = '#0000FF'; // bleu II
	$palette[1][9] = '#00FFFF'; // bleu III
	
	$palette[2][0] = '#FFFFFF'; //rouge
	$palette[2][1] = '#FFFFFF'; //orange
	$palette[2][2] = '#FFFFFF'; // jaune
	$palette[2][3] = '#FFFFFF'; // vert I
	$palette[2][4] = '#FFFFFF'; // bleu I
	$palette[2][5] = '#FFFFFF'; // mauve
	$palette[2][6] = '#FFFFFF'; // violet
	$palette[2][7] = '#FFFFFF'; // vert II
	$palette[2][8] = '#FFFFFF'; // bleu II
	$palette[2][9] = '#FFFFFF'; // bleu III
	
	
	/*$palette[0]['r'] = 247; // rouge
	$palette[0]['g'] = 188;
	$palette[0]['b'] = 192;
			
	$palette[1]['r'] = 254; // orange
	$palette[1]['g'] = 235;
	$palette[1]['b'] = 205;
		
	$palette[2]['r'] = 254; // jaune
	$palette[2]['g'] = 255;
	$palette[2]['b'] = 203;
	
	$palette[3]['r'] = 220; // vert I
	$palette[3]['g'] = 255;
	$palette[3]['b'] = 203;
	
	$palette[4]['r'] = 210; // bleu I
	$palette[4]['g'] = 234;
	$palette[4]['b'] = 255;

	$palette[5]['r'] = 218; // violet
	$palette[5]['g'] = 205;
	$palette[5]['b'] = 255;

	
	$palette[6]['r'] = 241; // rose
	$palette[6]['g'] = 198;
	$palette[6]['b'] = 222;
	
	$palette[7]['r'] = 237; // vert II
	$palette[7]['g'] = 255;
	$palette[7]['b'] = 200;
	
	$palette[8]['r'] = 208; // bleu II
	$palette[8]['g'] = 216;
	$palette[8]['b'] = 255;
	
	$palette[9]['r'] = 209; // bleu III
	$palette[9]['g'] = 255;
	$palette[9]['b'] = 255;*/
	
	
	/***********************************
	RÉCUPÉRATION DES COURS DANS UN ARRAY
	***********************************/
	if(!isset($_SESSION['membre_id'])){
		//Sécurité !
		include('bdd.php');
		$mail= mysql_real_escape_string(nl2br(htmlspecialchars($_POST['mail'])));
		
		$mail = preg_replace("#\.\.\.\\\\r\\\\n#", '...', $mail);
		//enregistrement dans un premier array
		$mail = preg_replace("#([A-Z0-9]{4}(\s)+[CDT](\s*[0-9])*(\s)+)((LUNDI|MARDI|MERCREDI|JEUDI|VENDREDI|SAMEDI|DIMANCHE)\.*(\s)+[0-9]{1,2}:[0-9]{1,2}-(\s)*[0-9]{1,2}:[0-9]{1,2},F[0-9],S=[A-Z0-9\s]+(\s)+)/(((LUNDI|MARDI|MERCREDI|JEUDI|VENDREDI|SAMEDI|DIMANCHE)\.*(\s)+[0-9]{1,2}:[0-9]{1,2}-(\s)*[0-9]{1,2}:[0-9]{1,2},F[0-9],S=[A-Z0-9\s]+))+#", '$1$5<br/>$1$10', $mail);
	
	
		
		$format = "#[A-Z0-9]{4}(\s)+[CDT](\s*[0-9])*(\s)+(LUNDI|MARDI|MERCREDI|JEUDI|VENDREDI|SAMEDI|DIMANCHE)\.*(\s)+[0-9]{1,2}:[0-9]{1,2}-(\s)*[0-9]{1,2}:[0-9]{1,2},F[0-9],S=[A-Z0-9\s]+#";
		preg_match_all($format, $mail, $heures_tamp);
		$detail=array();

		// scindage de l'array en "sous-array"
		$i=0;
		foreach($heures_tamp[0] as $ligne){
			
			// uv
			preg_match("#[A-Z0-9]{4}#", $ligne, $case);
			$detail['uv'] = $case[0];
			
			// type (cours, TD ou TP)
			preg_match("#\s[CDT](\s*[0-9])*#", $ligne, $case);
			preg_match("#[CDT]#", $case[0], $case);
			$detail['type'] = $case[0];
			
			// groupe du TD ou TP
			preg_match("#\s[CDT](\s*[0-9])*#", $ligne, $case);
			preg_match("#[0-9]+#", $case[0], $case);
			$detail['groupe'] = $case[0];
			if($detail['groupe'] == '')
				$detail['groupe'] = 0;
			
			// jour du cours
			preg_match("#(LUNDI|MARDI|MERCREDI|JEUDI|VENDREDI|SAMEDI|DIMANCHE)#", $ligne, $case);
			$detail['jour'] = $case[0];
			
			if($detail['jour'] == 'LUNDI')
				$detail['n_jour'] = 0;
			elseif($detail['jour'] == 'MARDI')
				$detail['n_jour'] = 1;
			elseif($detail['jour'] == 'MERCREDI')
				$detail['n_jour'] = 2;
			elseif($detail['jour'] == 'JEUDI')
				$detail['n_jour'] = 3;
			elseif($detail['jour'] == 'VENDREDI')
				$detail['n_jour'] = 4;
			elseif($detail['jour'] == 'SAMEDI')
				$detail['n_jour'] = 5;
			
			// heures de début et de fin
			preg_match("#([0-9]{1,2}):([0-9]{1,2})-[\s]*([0-9]{1,2}):([0-9]{1,2})#", $ligne, $case);
			$detail['h_deb'] = $case[1];
			$detail['m_deb'] = $case[2];
			$detail['h_fin'] = $case[3];
			$detail['m_fin'] = $case[4];
			
			// durée
			$duree_h = $detail['h_fin'] - $detail['h_deb']; 
			$duree_m = $detail['m_fin'] - $detail['m_deb'];
			if($duree_m < 0){
				$duree_h--;
				$duree_m = $duree_m + 60;
			}
			if($duree_m == 0)
				$duree_m = '00';
			
			// fréquence : toutes les semaines ou deux
			preg_match("#F[0-9]#", $ligne, $case);
			preg_match("#[0-9]#", $case[0], $case);
			$detail['frequence'] = $case[0];
			
			if($detail['frequence'] != 1)
				$detail['semaine'] = 1;
			elseif($detail['frequence'] == 1)
				$detail['semaine'] = 0;
			
			// salle
			preg_match("#=[A-Z0-9]+#", $ligne, $case);
			preg_match("#[A-Z0-9]+#", $case[0], $case);
			$detail['salle'] = $case[0];
			
			
			
			
			//Exceptions
			if($detail['salle'] == 'XXXXX')
				$detail['salle'] = '0';
			
			
			//enregistre toutes les données de l'emploi du temps dans un array
			if($detail['uv'] != "SPJE"){
				$edt[$i] = array (
					'uv'		=> $detail['uv'],
					'type' 		=> $detail['type'],
					'groupe'	=> $detail['groupe'],
					'jour'		=> $detail['jour'],
					'n_jour'	=> $detail['n_jour'],
					'h_deb'		=> $detail['h_deb'],
					'm_deb'		=> $detail['m_deb'],
					'deb'		=> $detail['h_deb']*100+$detail['m_deb'],
					'h_fin'		=> $detail['h_fin'],
					'm_fin'		=> $detail['m_fin'],
					'fin'		=> $detail['h_fin']*100+$detail['m_fin'],
					'duree_h'	=> $duree_h,
					'duree_m'	=> $duree_m,
					'frequence'	=> $detail['frequence'],
					'semaine'	=> $detail['semaine'],
					'salle'		=> $detail['salle']
				);	
				$i++;
			}
			
		}
	}
	elseif(isset($_SESSION['membre_id'])){
			
		$retour_liste_cours = mysql_query("SELECT * FROM cours WHERE email ='". $_SESSION['membre_id'] ."'");
	
		$i=0;
	
		while($ligne = mysql_fetch_array($retour_liste_cours)){
			
			//enregistre toutes les données de l'emploi du temps dans un array
			if($ligne['uv'] != "SPJE"){
				$ligne['uv'] = preg_replace("#\s#", "_", $ligne['uv']);
				$edt[$i] = array (
					'uv'		=> $ligne['uv'],
					'type' 		=> $ligne['type'],
					'groupe'	=> $ligne['groupe'],
					'jour'		=> $ligne['jour'],
					'n_jour'	=> $ligne['n_jour'],
					'h_deb'		=> $ligne['h_deb'],
					'm_deb'		=> $ligne['m_deb'],
					'deb'		=> $ligne['deb'],
					'h_fin'		=> $ligne['h_fin'],
					'm_fin'		=> $ligne['m_fin'],
					'fin'		=> $ligne['fin'],
					'duree_h'	=> $ligne['duree_h'],
					'duree_m'	=> $ligne['duree_m'],
					'frequence'	=> $ligne['frequence'],
					'semaine'	=> $ligne['semaine'],
					'salle'		=> $ligne['salle'],
					'afficher'	=> $ligne['afficher'],
					'empreinte'	=> $ligne['empreinte']
				);	
				$i++;
			}
			
		}
	}
	/*****************************
	RÉCUPÉRATION DU TITRE DE L'EDT
	*****************************/
	
	// Titre principal par défaut
	
	$titre_defaut = "Emploi du temps";
	
	// Niveau (TCxx ou GXxx)
	
	/*$format = "#(TC|G[A-Z]{1,2})[0-9][1-9]#";
	preg_match($format, $mail, $niveau);
	
	// Semestre (Axx ou Pxx)
	
	$annee = date('Y');
	
	$mois = date('m');
	if($mois >= 2 && $mois < 8){
		$semestre = 'Printemps ' . $annee;
	}
	else{
		$semestre = 'Automne ' . $annee;
	}
	
	// Prénom
	
	preg_match("#([a-z\-]+)\.([a-z\-]+)@etu\.utc\.fr#", $mail, $email);
	
	$prenom = preg_replace("#-#", " ", $email[1]);
	$prenom = ucwords($prenom);
	
	$nom = preg_replace("#-#", " ", $email[2]);
	$nom = ucwords($nom);

	
	if($prenom != '' || $nom != '')
		$titre_B = $prenom . ' ' . $nom . ' - ';
	if($niveau[0] != '')
		$titre_B = $titre_B . $niveau[0] . ' - ';
	
	$titre_B = $titre_B . $semestre;

	// E-mail
	
	$email = $email[0];*/
	$col_cadre="";
	if(isset($_SESSION['membre_id'])){
		$retour_liste_graphique = mysql_query("SELECT * FROM graphique WHERE login ='". $_SESSION['membre_id'] ."'");
		$ligne = mysql_fetch_array($retour_liste_graphique);
		
		if($ligne != NULL){
			$titre_A = $ligne['titre'];
			$titre_B = $ligne['soustitre'];
			for($i=0; $i <= 6; $i++)
				$col_cadre .= $ligne['cadre'][$i];
			$colrgb_cadre = hex_rgb($col_cadre);
			$txt_cadre = $ligne['cadre'][8];
			$para_grille = $ligne['grille'];
			$para_align = $ligne['align'];
			$para_groupe = $ligne['groupe'];
			$para_orientation = $ligne['orientation'];
			
			$i = 0;
			$j = 0;
			while($j<10){
		
				for($i=0; $i <= 6; $i++)
					$palette[0][$j] .= $ligne['cases'][$j* 10 + $i];
				$txt_cases[$j] = $ligne['cases'][$j* 10 + 8];
				
				$j++;
				
			}

		}
		
	}
	else{
		$col_cadre = '#E6E6E6';
		$colrgb_cadre = array(230,230, 230);
		$txt_cadre = 0;
		$para_grille = 0;
		$para_align = 'g';
		$para_groupe = 0;
		$para_orientation = 0;
		for($i=0; $i < 9; $i++){
			$palette[3][$i] = $palette[0][$i];
			$txt_cases[$i] = 0;
		}
	}
	

	/***************
	LISTE DES DES UV
	***************/
	
	$i = 0;
	$liste_uv = array ();
	if(is_array($edt)){
	foreach($edt as $ligne){
		$tampon = 1;
		foreach($liste_uv as $uv_courante){
			if($uv_courante == $ligne['uv'])
				$tampon = 0;
		}
		if($tampon == 1){
			$liste_uv[$i] = $ligne['uv'];
			$i++;
		}	
	}
	}
	

	/************************************
	AFFICHAGE DU MENU DE PERSONNALISATION
	************************************/
	

	
	
	$str_edt = serialize($edt);
	//echo $str_edt;
	$str_edt = preg_replace("#\"#", "/", $str_edt);
	//echo $str_edt;
	
	$str_uv = serialize($liste_uv);
	//echo $str_edt;
	$str_uv = preg_replace("#\"#", "/", $str_uv);
	//echo $str_edt;
	
	include('banniere.php'); 
	echo '<h2>Étape 2 : paramètres</h2><div id="corps">';
	
	echo '<form action="etape3.php" method="post">';
	
	
	///// CARRE INSCRIPTION /////
	if(!isset($_SESSION['membre_id'])){
		echo '<fieldset><legend>Inscripiton</legend>';
		
		
		echo '<p class="orange">Si tu choisis de t\'inscrire, tu auras la possibilité de voir les cours que tu as en commun avec les ' . Nb_membres() .' autres utcéens inscrits et de modifier ton emploi du temps (ajout d\'entretiens, changement de la semaine de TP, etc.). N\'est-ce pas merveilleux ?!</p>';
		
		echo '<input type="checkbox" name="inscription" id="inscri"/> <label for="inscri">Je m\'inscris ! </label>';
		echo 'Adresse mail UTC (prenom.nom@etu.utc.fr) : <input type="input" name="email" value="'. $email .'"/>';
		
		echo '<p class="orange">Attention ! Ton emploi du temps sera alors enregistré dans la base de donnée et consultable par les autres personnes inscrites.</p>';
		
		echo '</fieldset>';
	}
	///// CARRE PARAMETRES GENERAUX /////
	
	echo '<fieldset><legend>Paramètres généraux</legend>';
	
		echo '<table><tr><td>Titre :</td><td><input type="input" name="titre_A" value="'. $titre_defaut .'"></td></tr>';
		echo '<tr><td>Sous-titre :</td><td><input type="input" name="titre_B" value="'. $titre_B .'"></td></tr>';
		echo '<tr><td>Couleur du cadre :</td><td>';
		$x=666;
		
		

		
		
		
		
		
		
?>
					<img id="myRainbow<?=$x?>" class="color_wheel" alt="[r]" src="color_wheel.gif" />
					<input type="hidden" name="backgroundColor<?=$x?>" id="inputColor<?=$x?>" value="#FFFFFF" />
					
					<span class="background" id="bgColor<?=$x?>" style="background-color:<?=$col_cadre?>">&#160;</span>

					<input id="color<?=$x?>" type="hidden" size="8" name="color_cadre" onChange="colorChanging(<?=$x?>,this.value);" value="<?=$col_cadre?>" />

					<script type="text/javascript">
						window.addEvent('load',function(){addNewMooRainbow(<?=$x?>,<?=$colrgb_cadre[0]?>,<?=$colrgb_cadre[1]?>,<?=$colrgb_cadre[2]?>)});
					 
					</script>					
										
					
<?php
		
		echo '</td></tr>';
		echo '<tr><td>Couleur du texte : </td><td>';
		echo '<select name="color_cadre_txt">';
			echo '<option value="0" ';
				if($txt_cadre == 0){ echo 'selected="selected"'; }
			echo '>Automatique</option>';
			echo '<option value="1" ';
				if($txt_cadre == 1){ echo 'selected="selected"'; }
			echo '>Noir</option>';
			echo '<option value="2" ';
				if($txt_cadre == 2){ echo 'selected="selected"'; }
			echo '>Blanc</option>';
		echo '</select></td></tr>';


		$x = 10;/*
?>		
		<img id="myRainbow10" class="color_wheel" alt="[r]" src="color_wheel.gif" />
		<span class="background" id="bgColor10" >&#160;</span>
		<input id="color10" type="text" size="8" name="color_0" onChange="colorChanging(10,this.value);" value="#FFFFFF" />
		
		<script type="text/javascript">
			window.addEvent('load',function(){addNewMooRainbow(<?=$x?>)});
		</script>


<?php*/


		echo '<tr><td>Précision sur la grille de fond :</td><td><select name="grille">';
			echo '<option value="0" ';
				if($para_grille == 0){ echo 'selected="selected"'; }
			echo '>60 minutes</option>';
			echo '<option value="30" ';
				if($para_grille == 30){ echo 'selected="selected"'; }
			echo '>30 minutes</option>';
			echo '<option value="15" ';
				if($para_grille == 15){ echo 'selected="selected"'; }
			echo '>15 minutes</option>';
		echo '</select></td></tr>';
		echo '<tr><td>Orientation :</td><td><select name="orientation">';
			echo '<option value="0" ';
				if($para_orientation == 0){ echo 'selected="selected"'; }
			echo '>Vertical</option>';
			echo '<option value="1" ';
				if($para_orientation == 1){ echo 'selected="selected"'; }
			echo '>Horizontal</option>';
		echo '</select></td></tr></label>';
		
		// jeu de couleur par défaut
		$color=array();

		for($i = 0; $i < 3; $i++){
			for($j=0; $j < 10; $j++){
				if(!isset($color[$i]))
					$color[$i] = '';
				$color[$i] .= 'colorChanging(' . $j . ',\''. $palette[$i][$j] .'\');tabMooRainbow['. $j .'].manualSet(\''. $palette[$i][$j] . '\', \'hex\');document.getElementById(\'color' . $j .'\').value=\''. $palette[$i][$j] .'\';';
			}
		}		
		echo '<tr><td>Jeu de couleurs par défaut:</td><td><select name="jeu_col">';
			echo '<option value="claire" onclick="' . $color[0] . '" >Couleurs claires</option>';
			echo '<option value="vive" onclick="' . $color[1] . '">Couleurs vives</option>';
			echo '<option value="blanc" onclick="' . $color[2] . '">Tout blanc</option>';
			
		echo '</select></td></tr>';
		
		echo '<tr><td>Alignement du texte : </td><td>';
		echo '<select name="alignement">';
			echo '<option value="0" ';
				if($para_align == 'g'){ echo 'selected="selected"'; }
			echo '>Aligné en haut à gauche</option>';
			echo '<option value="1" ';
				if($para_align == 'c'){ echo 'selected="selected"'; }
			echo '>Aligné au centre</option>';
		echo '</select></td></tr>';
		
		echo '<tr><td></td<td><input type="checkbox" name="grp_TD" id="grp_TD"  ';
				if($para_groupe == 1){ echo 'checked="checked"'; }
			echo '/> <label for="grp_TD">Afficher le groupe de TD</td></tr></label>';
		
		echo '</tr></table>';
		
		
	echo '</fieldset>';
	echo '<fieldset><legend>Paramètres des UV et des cours, TD, TP et entretiens</legend>';
	
	
	
	echo '<table class="par_uv">';
	
	$i = 0;
	$j = 0;
	$k =0;
	$x=0;
	foreach($liste_uv as $ligne){
		//echo '<fieldset><legend>Paramètres pour ' . $ligne .'</legend>';
		echo '<tr><td>' . $ligne . '</td><td style="padding: 8px;">';
		echo 'Couleur personnalisée : ';
?>
					<img id="myRainbow<?=$x?>" class="color_wheel" alt="[r]" src="color_wheel.gif" />
					<input type="hidden" name="backgroundColor<?=$x?>" id="inputColor<?=$x?>" value="#FFFFFF" />
					
					<span class="background" id="bgColor<?=$x?>" style="background-color:<?=$palette[3][$x]?>">&#160;</span>

					<input id="color<?=$x?>" type="hidden" size="8" name="color_<?=$ligne?>" onChange="colorChanging(<?=$x?>,this.value);" value="<?=$palette[3][$x]?>" />

					<script type="text/javascript">
						window.addEvent('load',function(){addNewMooRainbow(<?=$x?>,<?php echo hexdec($palette[3][$x][1] . $palette[3][$x][2]);?>, <?php echo hexdec($palette[3][$x][3] . $palette[3][$x][4]);?>, <?php echo hexdec($palette[3][$x][5] . $palette[3][$x][6]);?>)});
						
						
						
					function affiche_span(nom_span, nom_spanAffiche, nom_spanCache) {
						document.getElementById(nom_span).style.display="block";
						document.getElementById(nom_spanAffiche).style.display="none";
						document.getElementById(nom_spanCache).style.display="block";
						
					} 
					function cache_span(nom_span, nom_spanAffiche, nom_spanCache) {
						document.getElementById(nom_span).style.display="none";
						document.getElementById(nom_spanAffiche).style.display="block";
						document.getElementById(nom_spanCache).style.display="none";
						
					}  
					</script>					
										
					
<?php


		$x++;
		$j++;
		echo '<br/>Couleur du texte : ';
		echo '<select name="colortxt'. $k .'">';
			echo '<option value="0" ';
				if($txt_cases[$k] == 0){ echo 'selected="selected"'; }
			echo '>Automatique</option>';
			echo '<option value="1" ';
				if($txt_cases[$k] == 1){ echo 'selected="selected"'; }
			echo '>Noir</option>';
			echo '<option value="2" ';
				if($txt_cases[$k] == 2){ echo 'selected="selected"'; }
			echo '>Blanc</option>';
		echo '</select>';
		if(!isset($_SESSION['membre_id'])){
			echo '<div style="font-size:10pt;text-decoration:underline;margin-top:10px" id="spanAffiche_'. $k .'"onclick="affiche_span(\'span_'. $k .'\', \'spanAffiche_'. $k .'\' , \'spanCache_'. $k .'\')">Afficher les autres paramètres (masquer un cours, modifier la semaine, le jour, l\'heure etc.)</div>';
			echo '<div style="font-size:10pt;text-decoration:underline;margin-top:10px" id="spanCache_'. $k .'"onclick="cache_span(\'span_'. $k .'\', \'spanAffiche_'. $k .'\' , \'spanCache_'. $k .'\')">Cacher</div>';
			echo '<span id="span_'. $k .'">';
		
		
		
			echo '<table style="margin-top: 8px;">';
			echo '<tr><th>cours, TD ou TP</th><th>jour</th><th>heure de début</th><th>durée</th><th>semaine</th><th>salle</th><th>Afficher</th></tr>';
			
			foreach($edt as $ligneb){
				if($ligneb['uv'] == $ligne){
					echo '<tr>';
					
					// Type
					echo '<td><select name="'. $i .'_type">';
						echo '<option value="C" ';
							if($ligneb['type'] == 'C'){ echo 'selected="selected"'; }
						echo '>Cours</option>';
						
						echo '<option value="D" ';
							if($ligneb['type'] == 'D'){ echo 'selected="selected"'; }
						echo '>TD</option>';
						
						echo '<option value="T" ';
							if($ligneb['type'] == 'T'){ echo 'selected="selected"'; }
						echo '>TP</option>';
						
						echo '<option value="E" ';
							if($ligneb['type'] == 'E'){ echo 'selected="selected"'; }
						echo '>Entretien</option>';
						
					echo '</select></td>';
					
					// Jour
					echo '<td><select name="'. $i .'_jour">';
						echo '<option value="LUNDI" ';
							if($ligneb['jour'] == 'LUNDI'){ echo 'selected="selected"'; }
						echo '>Lundi</option>';
						
						
						echo '<option value="MARDI" ';
							if($ligneb['jour'] == 'MARDI'){ echo 'selected="selected"'; }
						echo '>Mardi</option>';	
						
						
						echo '<option value="MERCREDI" ';
							if($ligneb['jour'] == 'MERCREDI'){ echo 'selected="selected"'; }
						echo '>Mercredi</option>';
						
						
						echo '<option value="JEUDI" ';
							if($ligneb['jour'] == 'JEUDI'){ echo 'selected="selected"'; }
						echo '>Jeudi</option>';
						
						
						echo '<option value="VENDREDI" ';
							if($ligneb['jour'] == 'VENDREDI'){ echo 'selected="selected"'; }
						echo '>Vendredi</option>';
			
			
						echo '<option value="SAMEDI" ';
							if($ligneb['jour'] == 'SAMEDI'){ echo 'selected="selected"'; }
						echo '>Samedi</option>';
					echo '</select></td>';
			
					// heure de début
					echo '<td><input class="XS" type="text" name="'. $i .'_h_deb" value="'. $ligneb['h_deb'] .'"/>h<input class="XS" type="text" name="'. $i .'_m_deb" value="'. $ligneb['m_deb'] .'"/></td>';
					
					// durée
					echo '<td><input class="XS" type="text" name="'. $i .'_duree_h" value="'. $ligneb['duree_h'] .'"/>h<input class="XS" type="text" name="'. $i .'_duree_m" value="'. $ligneb['duree_m'] .'"/></td>';
					
					// fréquence
					echo '<td><select name="'. $i .'_frequence">';
						echo '<option value="0" ';
						if($ligneb['semaine'] == 0){ echo 'selected="selected"'; }
						echo '>Toutes les semaines</option>';
						
						echo '<option value="1" ';
						if($ligneb['semaine'] == 1){ echo 'selected="selected"'; }
						echo '>Semaines A</option>';
						
						echo '<option value="2" ';
						if($ligneb['semaine'] == 2){ echo 'selected="selected"'; }
						echo '>Semaines B</option>';
					echo '</select></td>';
					
					// salle
					echo '<td><input type="text" name="'. $i .'_salle" value="'. $ligneb['salle'] .'"/></td>';
					
					// affichage
					echo '<td><input type="checkbox" name="'. $i .'_afficher" checked="checked"></td>';
					echo '</tr>';
					
					$i++;
				}
				
			}
			echo '</table></td></tr>';
		}
		
		echo '</span>';
		$k++;
		
	}
	
	echo '</table>';
	echo '</fieldset>';
/*	
	echo '<fieldset><legend>Paramètres des cours, TD et TP</legend>';
	
	echo '<table>';
	echo '<tr><th>UV</th><th>cours, TD ou TP</th><th>jour</th><th>heure de début</th><th>durée</th><th>semaine</th><th>salle</th><th>Afficher</th></tr>';
	
	
	// Personnalisation des cours
	$i = 0;
	foreach($edt as $ligne){
		
		echo '<tr>';
		
		// UV
		echo '<td>'. $ligne['uv'] .'</td>';
		
		// Type
		echo '<td><select name="'. $i .'_type">';
			echo '<option value="C" ';
				if($ligne['type'] == 'C'){ echo 'selected="selected"'; }
			echo '>Cours</option>';
			
			echo '<option value="D" ';
				if($ligne['type'] == 'D'){ echo 'selected="selected"'; }
			echo '>TD</option>';
			
			echo '<option value="T" ';
				if($ligne['type'] == 'T'){ echo 'selected="selected"'; }
			echo '>TP</option>';
			
			echo '<option value="E" ';
				if($ligne['type'] == 'E'){ echo 'selected="selected"'; }
			echo '>Entretien</option>';
			
		echo '</select></td>';
		
		// Jour
		echo '<td><select name="'. $i .'_jour">';
			echo '<option value="LUNDI" ';
				if($ligne['jour'] == 'LUNDI'){ echo 'selected="selected"'; }
			echo '>Lundi</option>';
			
			
			echo '<option value="MARDI" ';
				if($ligne['jour'] == 'MARDI'){ echo 'selected="selected"'; }
			echo '>Mardi</option>';	
			
			
			echo '<option value="MERCREDI" ';
				if($ligne['jour'] == 'MERCREDI'){ echo 'selected="selected"'; }
			echo '>Mercredi</option>';
			
			
			echo '<option value="JEUDI" ';
				if($ligne['jour'] == 'JEUDI'){ echo 'selected="selected"'; }
			echo '>Jeudi</option>';
			
			
			echo '<option value="VENDREDI" ';
				if($ligne['jour'] == 'VENDREDI'){ echo 'selected="selected"'; }
			echo '>Vendredi</option>';


			echo '<option value="SAMEDI" ';
				if($ligne['jour'] == 'SAMEDI'){ echo 'selected="selected"'; }
			echo '>Samedi</option>';
		echo '</select></td>';

		// heure de début
		echo '<td><input class="XS" type="text" name="'. $i .'_h_deb" value="'. $ligne['h_deb'] .'"/>h<input class="XS" type="text" name="'. $i .'_m_deb" value="'. $ligne['m_deb'] .'"/></td>';
		
		// durée
		echo '<td><input class="XS" type="text" name="'. $i .'_duree_h" value="'. $ligne['duree_h'] .'"/>h<input class="XS" type="text" name="'. $i .'_duree_m" value="'. $ligne['duree_m'] .'"/></td>';
		
		// fréquence
		echo '<td><select name="'. $i .'_frequence">';
			echo '<option value="0" ';
			if($ligne['semaine'] == 0){ echo 'selected="selected"'; }
			echo '>Toutes les semaines</option>';
			
			echo '<option value="1" ';
			if($ligne['semaine'] == 1){ echo 'selected="selected"'; }
			echo '>Semaines A</option>';
			
			echo '<option value="2" ';
			if($ligne['semaine'] == 2){ echo 'selected="selected"'; }
			echo '>Semaines B</option>';
		echo '</select></td>';
		
		// salle
		echo '<td><input type="text" name="'. $i .'_salle" value="'. $ligne['salle'] .'"/></td>';
		
		// affichage
		echo '<td><input type="checkbox" name="'. $i .'_afficher" checked="checked"></td>';
		echo '</tr>';
		
		$i++;
	}
	
	echo '</table>';
	echo '</fieldset>';
	*/
		echo '<input type="hidden" name="edt" value="'. $str_edt .'">';
		echo '<input type="hidden" name="uv" value="'. $str_uv .'">';
		//echo '<input type="hidden" name="nom" value="'. $nom .'">';
		//echo '<input type="hidden" name="prenom" value="'. $prenom .'">';
		echo '<br/><input type="submit" value="';
			if(isset($_SESSION['membre_id'])){ echo 'Enregistrer et afficher l\'emploi du temps'; }
			else{ echo 'Continuer (résultat)'; }
		echo '" />';
			
	echo '<form>';	
}
else{
	echo 'Le mail d\'emploi du temps n\'a pas été copié dans le formulaire !';
}
?>

</div><?php include("pied.php");?>
	</body>
</html>
