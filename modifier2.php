<?php 
/*
Copyright (C) 2011  Thomas SICARD - sicard.th@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 *	modifier2.php
 *	Modification de l'edt dans la base de donnée, à partir des données reçues par le formulaire de modifier.php
 *
 */


session_start();
include('bdd.php');
if(isset($_SESSION['membre_id'])){
	
/// MODIFIER ///

	
	
	
	
	
 	$titre_A = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['titre_A']))));
 	$titre_B = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['titre_B']))));
 	
 	$grille = $_POST['grille'];
 	
 	$col_cadre = array (
 		'r' => stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['cor_cadre'])))),
 		'g' => stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['cog_cadre'])))),
 		'b' => stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['cob_cadre'])))),
 		'txt' => 'black'
 	);
 	
 	
 	if(($col_cadre['r'] + $col_cadre['g'] + $col_cadre['b']) <= (3 * 128))
 		$col_cadre['txt'] = 'white';
 		
 	$a5 = 0;
	if(isset($_POST['a5']))
		$a5 = 1;
 	
 	// Récupération du tableau des cours et mise à jour
	$str_edt = preg_replace("#/#", "\"", $_POST['edt']);
	$edt = unserialize($str_edt);

	$nb_cours = 0;
	foreach($edt as $ligne){
		$nb_cours++; 
	}
	
	
	for($i = 0; $i < $nb_cours; $i++ ){
		
		$cor = -1; // correspondance
		for($j = 0; $j < $nb_cours; $j++){
			if($edt[$i]['empreinte'] == $_POST[$j . '_empreinte'])
				$cor = $j;
		}
		
		
		
		
		// Cours, TD, TP
		$edt[$i]['type'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST[$cor . '_type']))));
		
		// Jour
		$edt[$i]['jour'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST[$cor . '_jour']))));
		if($edt[$i]['jour'] == 'LUNDI')
			$edt[$i]['n_jour'] = 0;
		elseif($edt[$i]['jour'] == 'MARDI')
			$edt[$i]['n_jour'] = 1;
		elseif($edt[$i]['jour'] == 'MERCREDI')
			$edt[$i]['n_jour'] = 2;
		elseif($edt[$i]['jour'] == 'JEUDI')
			$edt[$i]['n_jour'] = 3;
		elseif($edt[$i]['jour'] == 'VENDREDI')
			$edt[$i]['n_jour'] = 4;
		elseif($edt[$i]['jour'] == 'SAMEDI')
			$edt[$i]['n_jour'] = 5;
		
		// Heure et durée
		$edt[$i]['h_deb'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST[$cor . '_h_deb']))));
		$edt[$i]['m_deb'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST[$cor . '_m_deb']))));
		$edt[$i]['duree_h'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST[$cor . '_duree_h']))));
		$edt[$i]['duree_m'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST[$cor . '_duree_m']))));
		
		$edt[$i]['h_fin'] = $edt[$i]['h_deb'] + $edt[$i]['duree_h'] ;
		$edt[$i]['m_fin'] = $edt[$i]['m_deb'] + $edt[$i]['duree_m'] ;
		if($edt[$i]['m_fin'] >= 60){
			$edt[$i]['m_fin'] = $edt[$i]['m_fin'] - 60;
			$edt[$i]['h_fin']++;
		}
		
		$edt[$i]['deb'] = $edt[$i]['h_deb'] * 100 + $edt[$i]['m_deb'];
		$edt[$i]['fin'] = $edt[$i]['h_fin'] * 100 + $edt[$i]['m_fin'];
		
		// Semaine
		$_POST[$cor . '_frequence'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST[$cor . '_frequence']))));
		
		if($_POST[$cor . '_frequence'] == 0){
			$edt[$i]['frequence'] = 1;
			$edt[$i]['semaine'] = 0;
		}
		if($_POST[$cor . '_frequence'] == 1){
			$edt[$i]['frequence'] = 2;
			$edt[$i]['semaine'] = 1;
		}
		if($_POST[$cor . '_frequence'] == 2){
			$edt[$i]['frequence'] = 2;
			$edt[$i]['semaine'] = 2;
		}
		
		// Salle
		$edt[$i]['salle'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST[$cor . '_salle']))));
		
		// Affichage
		if($_POST[$cor . '_afficher'] == 'on'){
			$edt[$i]['afficher'] = 1;
			
		}
		else{
			$edt[$i]['afficher'] = 0;
		}
		
		
		
		// Création de l'empreinte
		$empreinte = $edt[$i]['uv'];
		$empreinte .= $edt[$i]['n_jour'];
		$empreinte .= $edt[$i]['deb'];
		$empreinte .= $edt[$i]['salle'];
		$empreinte .= $edt[$i]['semaine'];
	
		

	
		// Enregistrement edt dans la base
		mysql_query("UPDATE cours SET  type='". $edt[$i]['type'] ."',groupe='". $edt[$i]['groupe'] ."',jour='". $edt[$i]['jour'] ."',n_jour='". $edt[$i]['n_jour'] ."',h_deb='". $edt[$i]['h_deb'] ."',m_deb='". $edt[$i]['m_deb'] ."',deb='". $edt[$i]['deb'] ."',h_fin='". $edt[$i]['h_fin'] ."',m_fin='". $edt[$i]['m_fin'] ."',fin='". $edt[$i]['fin'] ."',duree_h='". $edt[$i]['duree_h'] ."',duree_m='". $edt[$i]['duree_m'] ."',frequence='". $edt[$i]['frequence'] ."',semaine='". $edt[$i]['semaine'] ."',salle='". $edt[$i]['salle'] ."', afficher='". $edt[$i]['afficher'] ."',empreinte='". $empreinte ."' WHERE email='". $_SESSION['membre_id'] ."' AND empreinte='". $edt[$i]['empreinte'] ."'");
	}


	
/// SUPPRIMER ///

	for($i = 0; $i < $nb_cours; $i++ ){
		
		if($_POST[$i . '_supprimer'] == 'on'){
			mysql_query("DELETE FROM cours WHERE email='". $_SESSION['membre_id'] ."' AND empreinte='". $edt[$i]['empreinte'] ."'");
		}
	}

/// AJOUTER ///  /// ATENTION, code dégueu !
	
	for($i=0; $i < 20; $i++){
		
		$edt_ajout[$i]['uv'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['ajouter_' . $i . '_uv']))));
		
		if($edt_ajout[$i]['uv'] != ''){

			// UV
			//cf au dessus
			
			// Cours, TD, TP
			$edt_ajout[$i]['type'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['ajouter_' . $i . '_type']))));
			
			// Jour
			$edt_ajout[$i]['jour'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['ajouter_' . $i . '_jour']))));
			if($edt_ajout[$i]['jour'] == 'LUNDI')
				$edt_ajout[$i]['n_jour'] = 0;
			elseif($edt_ajout[$i]['jour'] == 'MARDI')
				$edt_ajout[$i]['n_jour'] = 1;
			elseif($edt_ajout[$i]['jour'] == 'MERCREDI')
				$edt_ajout[$i]['n_jour'] = 2;
			elseif($edt_ajout[$i]['jour'] == 'JEUDI')
				$edt_ajout[$i]['n_jour'] = 3;
			elseif($edt_ajout[$i]['jour'] == 'VENDREDI')
				$edt_ajout[$i]['n_jour'] = 4;
			elseif($edt_ajout[$i]['jour'] == 'SAMEDI')
				$edt_ajout[$i]['n_jour'] = 5;
			
			// Heure et durée
			$edt_ajout[$i]['h_deb'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['ajouter_' . $i . '_h_deb']))));
			$edt_ajout[$i]['m_deb'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['ajouter_' . $i . '_m_deb']))));
			$edt_ajout[$i]['duree_h'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['ajouter_' . $i . '_duree_h']))));
			$edt_ajout[$i]['duree_m'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['ajouter_' . $i . '_duree_m']))));
			
			if($edt_ajout[$i]['h_deb'] == '')
				$edt_ajout[$i]['h_deb'] = 8;
			if($edt_ajout[$i]['m_deb'] == '')
				$edt_ajout[$i]['m_deb'] = 0;
			if($edt_ajout[$i]['duree_h'] == '')
				$edt_ajout[$i]['duree_h'] = 1;
			if($edt_ajout[$i]['duree_m'] == '')
				$edt_ajout[$i]['duree_m'] = 0;
			
			
			$edt_ajout[$i]['h_fin'] = $edt_ajout[$i]['h_deb'] + $edt_ajout[$i]['duree_h'] ;
			$edt_ajout[$i]['m_fin'] = $edt_ajout[$i]['m_deb'] + $edt_ajout[$i]['duree_m'] ;
			if($edt_ajout[$i]['m_fin'] >= 60){
				$edt_ajout[$i]['m_fin'] = $edt_ajout[$i]['m_fin'] - 60;
				$edt_ajout[$i]['h_fin']++;
			}
			
			$edt_ajout[$i]['deb'] = $edt_ajout[$i]['h_deb'] * 100 + $edt_ajout[$i]['m_deb'];
			$edt_ajout[$i]['fin'] = $edt_ajout[$i]['h_fin'] * 100 + $edt_ajout[$i]['m_fin'];
			
			// Semaine
			$_POST['ajouter_' . $i . '_semaine'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['ajouter_' . $i . '_semaine']))));
			
			if($_POST['ajouter_' . $i . '_semaine'] == 0){
				$edt_ajout[$i]['frequence'] = 1;
				$edt_ajout[$i]['semaine'] = 0;
			}
			if($_POST['ajouter_' . $i . '_semaine'] == 1){
				$edt_ajout[$i]['frequence'] = 2;
				$edt_ajout[$i]['semaine'] = 1;
			}
			if($_POST['ajouter_' . $i . '_semaine'] == 2){
				$edt_ajout[$i]['frequence'] = 2;
				$edt_ajout[$i]['semaine'] = 2;
			}
			
			// Salle
			$edt_ajout[$i]['salle'] = stripslashes(stripslashes(mysql_real_escape_string(htmlspecialchars($_POST['ajouter_' . $i . '_salle']))));
			if($edt_ajout[$i]['salle'] == '')
				$edt_ajout[$i]['salle'] = 0;
			
			// Affichage
			$edt_ajout[$i]['afficher'] = 1;
			$edt_ajout[$i]['confirm'] = 1;
			
			
			// Création de l'empreinte
			$empreinte = $edt_ajout[$i]['uv'];
			$empreinte .= $edt_ajout[$i]['n_jour'];
			$empreinte .= $edt_ajout[$i]['deb'];
			$empreinte .= $edt_ajout[$i]['salle'];
			$empreinte .= $edt_ajout[$i]['semaine'];
		
			// Enregistrement edt dans la base
			mysql_query("INSERT INTO cours VALUES('', '". $_SESSION['membre_id'] ."', '". $edt_ajout[$i]['uv'] ."', '". $edt_ajout[$i]['type'] ."', '". $edt_ajout[$i]['groupe'] ."', '". $edt_ajout[$i]['jour'] ."', '". $edt_ajout[$i]['n_jour'] ."', '". $edt_ajout[$i]['h_deb'] ."', '". $edt_ajout[$i]['m_deb'] ."', '". $edt_ajout[$i]['deb'] ."', '". $edt_ajout[$i]['h_fin'] ."', '". $edt_ajout[$i]['m_fin'] ."', '". $edt_ajout[$i]['fin'] ."', '". $edt_ajout[$i]['duree_h'] ."', '". $edt_ajout[$i]['duree_m'] ."', '". $edt_ajout[$i]['frequence'] ."', '". $edt_ajout[$i]['semaine'] ."', '". $edt_ajout[$i]['salle'] ."', '1', '". $empreinte ."', '1')");
		
		}
		$i++;
	}



	
}

	$_SESSION['info'] = 'Modifications effectuées';
	echo '<meta http-equiv="Refresh" content="0;URL=index.php">';	
?>