<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
	<head>
		<title>Emploi du temps</title>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	</head>
	<body>
		<div>
			Rentrez dans le cadre ci-dessous le contenu du mail envoy&eacute; par l'administration comprenant :
			<ul> 
				<li>Pour &eacute;metteur : "sme-noreply@utc.fr",</li>
				<li>Pour objet : "vos inscriptions",</li>
				<li>Et pour en-t&ecirc;te : "Nous avons le plaisir de vous faire parvenir la liste de vos inscriptions en UV".</li>
			</ul>
		</div>
		<br/>
		<form action="couleurs.php" method="post">
			<div id="form">
				<textarea name="texte_brut" cols="100" rows="20" >Rentrez le texte issu du mail ici.</textarea>
				<br /><br/>
				<input type="submit" value="Continuer" />
			</div>
		</form>
		<div id="signature">
			<br />
			<span>Nicolas Pauss, 2009.</span>
		</div>
	</body>
</html>
