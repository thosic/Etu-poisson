<?php
/*
Copyright (C) 2010-2011  Aurélien DUMAINE - aurelien.dumaine@etu.utc.fr

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//Connexion a la base de données
session_start();
include('bdd.php');

//On récupère l'ID de l'utilisateur par le coockie de session
$utilisateur = $_SESSION['membre_id'];
$table_calendrier = "calendrier_a10";

// Définition du fichier iCalendar
header("Content-Type: text/Calendar");
// Nom du fichier
header("Content-Disposition: inline; filename=agenda".date(dmyHis).".ics");
// Entête du fichier ical
echo "BEGIN:VCALENDAR
PRODID:Aurélien DUMAINE - icalGen - Université de Technologie de Compiègne
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-WR-CALNAME:".$table_calendrier."
X-WR-CALDESC:
BEGIN:VTIMEZONE
TZID:Europe/Paris
X-LIC-LOCATION:Europe/Paris
BEGIN:DAYLIGHT
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
TZNAME:CEST
DTSTART:19700329T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
TZNAME:CET
DTSTART:19701025T030000
RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU
END:STANDARD
END:VTIMEZONE\n";

//Trouver évènements recurrents de l'utilisateur pour le semestre en cours
$evenements_recurrents = mysql_query("SELECT * FROM cours WHERE (email='$utilisateur')") or die ('Erreur SQL! '.mysql_error());

//Pour chaque élément recurrent on parcourt le calendrier des altérances pour trouver les dates des séances
while($evenement = mysql_fetch_assoc($evenements_recurrents))
{
		// créer la boucle de création des dates des occurences
		$jour = $evenement['jour'];
		$alternance = $evenement['semaine'];
		  if ($alternance == "0") //si c'est un évènement hebdomadaire
        {
            $instances_evenement = mysql_query("SELECT date FROM $table_calendrier WHERE (jour_utc='$jour' AND (alternance='1' OR alternance='2')) ORDER BY date DESC") or die ('Erreur SQL! '.mysql_error());
        }
        else //si c'est un évènement de semaine A ou B
        {
            $instances_evenement = mysql_query("SELECT date FROM $table_calendrier WHERE (jour_utc='$jour' AND alternance='$alternance') ORDER BY date DESC") or die ('Erreur SQL! '.mysql_error());
        }

		//initialisation du compteur
		$i=1;
		
		$nb_instances = mysql_num_rows($instances_evenement);
		//formatage des dates et des heures
		if (strlen($evenement['deb'])==3)
		{
			$evenement['deb'] = "0".$evenement['deb'];
		}
		if (strlen($evenement['fin'])==3)
		{
			$evenement['fin'] = "0".$evenement['fin'];
		}	
		$heure_debut = $evenement['deb']."00";
		$heure_fin = $evenement['fin']."00";

		while($instance = mysql_fetch_assoc($instances_evenement))
		{
			//formatage de la date
			$instance['date'] = str_replace('-','',$instance['date']);
			
			if ($i==1) //en-tête de l'évènement
			{
			    echo "\nBEGIN:VEVENT\n";
			    echo "DTSTART;TZID=Europe/Paris:".$instance['date']."T".$heure_debut."\n";
			    echo "DTEND;TZID=Europe/Paris:".$instance['date']."T".$heure_fin."\n";
			    echo "RDATE:".$instance['date']."T".$heure_debut;
			}
			else //on intègre chaque occurence exeptée la première (dans l'en-tête)
			{
				echo $instance['date']."T".$heure_debut;
			}

			if ($i != ($nb_instances))
			{
				echo ",";
			}
			$i = $i+1;
		}

        echo "\nDESCRIPTION: Groupe:".$evenement['groupe']."\n";
        echo "LOCATION:".$evenement['salle']."\n";
	echo "SUMMARY:".$evenement['uv']."-".$evenement['type']."\n";
//l'UID doit-il être supprimer? ou géré autrement en cas d'import multiples?
//	echo "UID:{$evenement['id']}\n";
	echo "END:VEVENT\n"; 
}

echo "\n\nEND:VCALENDAR";

//fermeture de la connexion BDD
mysql_close($connexion);
?>
