<?php 
session_start();
include('bdd.php');

if(isset($_SESSION['membre_id'])){
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
	<head>
		<title>Étape 2 : paramètres</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" media="screen" type="text/css" title="Design" href="design.css" />
		<style type="text/css">
			textarea{
				display:block;
			}
			table{
				border-collapse: collapse;
			}
			.par_uv td, th{
				border : 1px solid black;
				padding : 2px;
			}
			.par_uv div{
				border: 1px solid black;
			}
			
			.XS{
				width: 30px;
			}
			
			/*body{
				background-image: url("fond.jpg");
				background-repeat : repeat-x;
				margin-left:0px;
				margin-top:0px;
			}

			h1{
				width:100%;
				background-color:white;
				margin-top:0px;
				padding: 4px;
				font-size: 20px;
				color: rgb(100,100,100);
				border-bottom: 1px solid rgb(100,100,100);
			}
			fieldset{
				border: 2px ridge yellow;
				margin: 4px;
				width: 1000px;
			}
			legend{
				color: rgb(70,70,70);
			}*/

		</style>
	</head>
	<body>

<?php


	/*****************
	PALETTE PAR DEFAUT
	*****************/

	$palette[0]['r'] = 247; // rouge
	$palette[0]['g'] = 188;
	$palette[0]['b'] = 192;
			
	$palette[1]['r'] = 254; // orange
	$palette[1]['g'] = 235;
	$palette[1]['b'] = 205;
		
	$palette[2]['r'] = 254; // jaune
	$palette[2]['g'] = 255;
	$palette[2]['b'] = 203;
	
	$palette[3]['r'] = 220; // vert I
	$palette[3]['g'] = 255;
	$palette[3]['b'] = 203;
	
	$palette[4]['r'] = 210; // bleu I
	$palette[4]['g'] = 234;
	$palette[4]['b'] = 255;

	$palette[5]['r'] = 218; // violet
	$palette[5]['g'] = 205;
	$palette[5]['b'] = 255;

	
	$palette[6]['r'] = 241; // rose
	$palette[6]['g'] = 198;
	$palette[6]['b'] = 222;
	
	$palette[7]['r'] = 237; // vert II
	$palette[7]['g'] = 255;
	$palette[7]['b'] = 200;
	
	$palette[8]['r'] = 208; // bleu II
	$palette[8]['g'] = 216;
	$palette[8]['b'] = 255;
	
	$palette[9]['r'] = 209; // bleu III
	$palette[9]['g'] = 255;
	$palette[9]['b'] = 255;
	
	/***********************************
	RÉCUPÉRATION DES COURS DANS UN ARRAY
	***********************************/
	
	
	$retour_liste_cours = mysql_query("SELECT * FROM cours WHERE email ='". $_SESSION['membre_id'] ."'");

	$i=0;
	while($ligne = mysql_fetch_array($retour_liste_cours)){
		
		//enregistre toutes les données de l'emploi du temps dans un array
		if($detail['uv'] != "SPJE"){
			$ligne['uv'] = preg_replace("#\s#", "_", $ligne['uv']);
			$edt[$i] = array (
				'uv'		=> $ligne['uv'],
				'type' 		=> $ligne['type'],
				'groupe'	=> $ligne['groupe'],
				'jour'		=> $ligne['jour'],
				'n_jour'	=> $ligne['n_jour'],
				'h_deb'		=> $ligne['h_deb'],
				'm_deb'		=> $ligne['m_deb'],
				'deb'		=> $ligne['deb'],
				'h_fin'		=> $ligne['h_fin'],
				'm_fin'		=> $ligne['m_fin'],
				'fin'		=> $ligne['fin'],
				'duree_h'	=> $ligne['duree_h'],
				'duree_m'	=> $ligne['duree_m'],
				'frequence'	=> $ligne['frequence'],
				'semaine'	=> $ligne['semaine'],
				'salle'		=> $ligne['salle'],
				'afficher'	=> $ligne['afficher'],
				'empreinte'	=> $ligne['empreinte']
			);	
			$i++;
		}
		
	}
	/*****************************
	RÉCUPÉRATION DU TITRE DE L'EDT
	*****************************/
	
	// Titre principal par défaut
	
	$titre_defaut = "Emploi du temps";
	
	// Niveau (TCxx ou GXxx)
	
	//
	// IMPOSSIBLE DE LE RETROUVER POUR L'INSTANT
	//
	
	// Semestre (Axx ou Pxx)
	
	$annee = date('Y');
	
	$mois = date('m');
	if($mois >= 2 && $mois < 8){
		$semestre = 'Printemps ' . $annee;
	}
	else{
		$semestre = 'Automne ' . $annee;
	}
	
	// Prénom
	
	preg_match("#([a-z\-]+)\.([a-z\-]+)@etu\.utc\.fr#", $_SESSION['membre_id'], $email);
	
	$prenom = preg_replace("#-#", " ", $email[1]);
	$prenom = ucwords($prenom);
	
	$nom = preg_replace("#-#", " ", $email[2]);
	$nom = ucwords($nom);

	
	if($prenom != '' || $nom != '')
		$titre_B = $prenom . ' ' . $nom . ' - ';
	if($niveau[0] != '')
		$titre_B = $titre_B . $niveau[0] . ' - ';
	
	$titre_B = $titre_B . $semestre;

	// E-mail
	
	$email = $email[0];
	
	/***************
	LISTE DES DES UV
	***************/
	
	$i = 0;
	$liste_uv = array ();
	foreach($edt as $ligne){
		$tampon = 1;
		foreach($liste_uv as $uv_courante){
			if($uv_courante == $ligne['uv'])
				$tampon = 0;
		}
		if($tampon == 1){
			$liste_uv[$i] = $ligne['uv'];
			$i++;
		}	
	}
	
	

	/************************************
	AFFICHAGE DU MENU DE PERSONNALISATION
	************************************/
	

	
	
	$str_edt = serialize($edt);
	//echo $str_edt;
	$str_edt = preg_replace("#\"#", "/", $str_edt);
	//echo $str_edt;
	
	$str_uv = serialize($liste_uv);
	//echo $str_edt;
	$str_uv = preg_replace("#\"#", "/", $str_uv);
	//echo $str_edt;
	
	echo '<h1><a href="index.php">Générateur d\'emplois du temps utc <span class="petit">(en attendant de trouver un autre nom…)</span></a></h1>';
	echo '<h2>Afficher mon emploi du temps</h2><div id="corps">';
	
	echo '<form action="etape3_compte.php" method="post">';
	
	///// CARRE PARAMETRES GENERAUX /////
	
	echo '<fieldset><legend>Paramètres généraux</legend>';
	
		echo '<table><tr><td>Titre :</td><td><input type="input" name="titre_A" value="'. $titre_defaut .'"></td></tr>';
		echo '<tr><td>Sous-titre :</td><td><input type="input" name="titre_B" value="'. $titre_B .'"></td></tr>';
		echo '<tr><td>Couleur du cadre (RGB) :</td><td><input class="XS"  type="input" name="cor_cadre" value="230"><input class="XS"  type="input" name="cog_cadre" value="230"><input class="XS"  type="input" name="cob_cadre" value="230"></td></tr>';
		echo '<tr><td>Précision sur la grille de fond :</td><td><select name="grille"><option value="0">60 minutes</option><option value="30">30 minutes</option><option value="15">15 minutes</option></select></td></tr>';
		echo '<tr><td></td<td><input type="checkbox" name="a5" id="a5" /> <label for="a5">Petit format (A5)</td></tr>';
		echo '<tr><td>Orientation</td<td><input type="radio" name="orientation" value="ver" id="Vertical" checked="checked" /> <label for="ver">Vertical</label><input type="radio" name="orientation" value="hor" id="hor" /> <label for="hor">Horizontal</label></td></tr></table></label>';
		
	echo '</fieldset>';
	echo '<fieldset><legend>Paramètres des UV et des cours, TD, TP et entretiens</legend>';

	echo '<table class="par_uv">';
	
	$i = 0;
	$j = 0;
	foreach($liste_uv as $ligne){
		$ligne_sans__ = preg_replace("#_#", " ", $ligne);
		$ligne = preg_replace("#\s#", "_", $ligne);
		//echo '<fieldset><legend>Paramètres pour ' . $ligne .'</legend>';
		echo '<tr><td>' . $ligne_sans__ . '</td><td style="padding: 8px;">';
		echo 'Couleur des cases (RGB) : <input class="XS"  type="input" name="cor_'. $ligne .'" value="'. $palette[$j]['r'] .'"><input class="XS"  type="input" name="cog_'. $ligne .'" value="'. $palette[$j]['g'] .'"><input class="XS"  type="input" name="cob_'. $ligne .'" value="'. $palette[$j]['b'] .'">';
		$j++;
		/*
		echo '<table style="margin-top: 8px;">';
		echo '<tr><th>cours, TD ou TP</th><th>jour</th><th>heure de début</th><th>durée</th><th>semaine</th><th>salle</th><th>Afficher</th></tr>';
		
		foreach($edt as $ligneb){
			if($ligneb['uv'] == $ligne){
				echo '<tr>';
				
				// Type
				echo '<td><select name="'. $i .'_type">';
					echo '<option value="C" ';
						if($ligneb['type'] == 'C'){ echo 'selected="selected"'; }
					echo '>Cours</option>';
					
					echo '<option value="D" ';
						if($ligneb['type'] == 'D'){ echo 'selected="selected"'; }
					echo '>TD</option>';
					
					echo '<option value="T" ';
						if($ligneb['type'] == 'T'){ echo 'selected="selected"'; }
					echo '>TP</option>';
					
					echo '<option value="E" ';
						if($ligneb['type'] == 'E'){ echo 'selected="selected"'; }
					echo '>Entretien</option>';
					
				echo '</select></td>';
				
				// Jour
				echo '<td><select name="'. $i .'_jour">';
					echo '<option value="LUNDI" ';
						if($ligneb['jour'] == 'LUNDI'){ echo 'selected="selected"'; }
					echo '>Lundi</option>';
					
					
					echo '<option value="MARDI" ';
						if($ligneb['jour'] == 'MARDI'){ echo 'selected="selected"'; }
					echo '>Mardi</option>';	
					
					
					echo '<option value="MERCREDI" ';
						if($ligneb['jour'] == 'MERCREDI'){ echo 'selected="selected"'; }
					echo '>Mercredi</option>';
					
					
					echo '<option value="JEUDI" ';
						if($ligneb['jour'] == 'JEUDI'){ echo 'selected="selected"'; }
					echo '>Jeudi</option>';
					
					
					echo '<option value="VENDREDI" ';
						if($ligneb['jour'] == 'VENDREDI'){ echo 'selected="selected"'; }
					echo '>Vendredi</option>';
		
		
					echo '<option value="SAMEDI" ';
						if($ligneb['jour'] == 'SAMEDI'){ echo 'selected="selected"'; }
					echo '>Samedi</option>';
				echo '</select></td>';
		
				// heure de début
				echo '<td><input class="XS" type="text" name="'. $i .'_h_deb" value="'. $ligneb['h_deb'] .'"/>h<input class="XS" type="text" name="'. $i .'_m_deb" value="'. $ligneb['m_deb'] .'"/></td>';
				
				// durée
				echo '<td><input class="XS" type="text" name="'. $i .'_duree_h" value="'. $ligneb['duree_h'] .'"/>h<input class="XS" type="text" name="'. $i .'_duree_m" value="'. $ligneb['duree_m'] .'"/></td>';
				
				// fréquence
				echo '<td><select name="'. $i .'_frequence">';
					echo '<option value="0" ';
					if($ligneb['semaine'] == 0){ echo 'selected="selected"'; }
					echo '>Toutes les semaines</option>';
					
					echo '<option value="1" ';
					if($ligneb['semaine'] == 1){ echo 'selected="selected"'; }
					echo '>Semaines A</option>';
					
					echo '<option value="2" ';
					if($ligneb['semaine'] == 2){ echo 'selected="selected"'; }
					echo '>Semaines B</option>';
				echo '</select></td>';
				
				// salle
				echo '<td><input type="text" name="'. $i .'_salle" value="'. $ligneb['salle'] .'"/></td>';
				
				// affichage
				echo '<td><input type="checkbox" name="'. $i .'_afficher" checked="checked"></td>';
				echo '</tr>';
				
				$i++;
			}
			
		}
		echo '</table>';*/
		echo '</td></tr>';

		
		
		
		
	}
	
	echo '</table>';
	echo '</fieldset>';
/*	
	echo '<fieldset><legend>Paramètres des cours, TD et TP</legend>';
	
	echo '<table>';
	echo '<tr><th>UV</th><th>cours, TD ou TP</th><th>jour</th><th>heure de début</th><th>durée</th><th>semaine</th><th>salle</th><th>Afficher</th></tr>';
	
	
	// Personnalisation des cours
	$i = 0;
	foreach($edt as $ligne){
		
		echo '<tr>';
		
		// UV
		echo '<td>'. $ligne['uv'] .'</td>';
		
		// Type
		echo '<td><select name="'. $i .'_type">';
			echo '<option value="C" ';
				if($ligne['type'] == 'C'){ echo 'selected="selected"'; }
			echo '>Cours</option>';
			
			echo '<option value="D" ';
				if($ligne['type'] == 'D'){ echo 'selected="selected"'; }
			echo '>TD</option>';
			
			echo '<option value="T" ';
				if($ligne['type'] == 'T'){ echo 'selected="selected"'; }
			echo '>TP</option>';
			
			echo '<option value="E" ';
				if($ligne['type'] == 'E'){ echo 'selected="selected"'; }
			echo '>Entretien</option>';
			
		echo '</select></td>';
		
		// Jour
		echo '<td><select name="'. $i .'_jour">';
			echo '<option value="LUNDI" ';
				if($ligne['jour'] == 'LUNDI'){ echo 'selected="selected"'; }
			echo '>Lundi</option>';
			
			
			echo '<option value="MARDI" ';
				if($ligne['jour'] == 'MARDI'){ echo 'selected="selected"'; }
			echo '>Mardi</option>';	
			
			
			echo '<option value="MERCREDI" ';
				if($ligne['jour'] == 'MERCREDI'){ echo 'selected="selected"'; }
			echo '>Mercredi</option>';
			
			
			echo '<option value="JEUDI" ';
				if($ligne['jour'] == 'JEUDI'){ echo 'selected="selected"'; }
			echo '>Jeudi</option>';
			
			
			echo '<option value="VENDREDI" ';
				if($ligne['jour'] == 'VENDREDI'){ echo 'selected="selected"'; }
			echo '>Vendredi</option>';


			echo '<option value="SAMEDI" ';
				if($ligne['jour'] == 'SAMEDI'){ echo 'selected="selected"'; }
			echo '>Samedi</option>';
		echo '</select></td>';

		// heure de début
		echo '<td><input class="XS" type="text" name="'. $i .'_h_deb" value="'. $ligne['h_deb'] .'"/>h<input class="XS" type="text" name="'. $i .'_m_deb" value="'. $ligne['m_deb'] .'"/></td>';
		
		// durée
		echo '<td><input class="XS" type="text" name="'. $i .'_duree_h" value="'. $ligne['duree_h'] .'"/>h<input class="XS" type="text" name="'. $i .'_duree_m" value="'. $ligne['duree_m'] .'"/></td>';
		
		// fréquence
		echo '<td><select name="'. $i .'_frequence">';
			echo '<option value="0" ';
			if($ligne['semaine'] == 0){ echo 'selected="selected"'; }
			echo '>Toutes les semaines</option>';
			
			echo '<option value="1" ';
			if($ligne['semaine'] == 1){ echo 'selected="selected"'; }
			echo '>Semaines A</option>';
			
			echo '<option value="2" ';
			if($ligne['semaine'] == 2){ echo 'selected="selected"'; }
			echo '>Semaines B</option>';
		echo '</select></td>';
		
		// salle
		echo '<td><input type="text" name="'. $i .'_salle" value="'. $ligne['salle'] .'"/></td>';
		
		// affichage
		echo '<td><input type="checkbox" name="'. $i .'_afficher" checked="checked"></td>';
		echo '</tr>';
		
		$i++;
	}
	
	echo '</table>';
	echo '</fieldset>';
	*/
		echo '<input type="hidden" name="edt" value="'. $str_edt .'">';
		echo '<input type="hidden" name="uv" value="'. $str_uv .'">';
		echo '<input type="hidden" name="nom" value="'. $nom .'">';
		echo '<input type="hidden" name="prenom" value="'. $prenom .'">';
		echo '<br/><input type="submit" value="Continuer (résultat)" />';
	echo '<form>';	


?>
</div>
	</body>
</html>



<?php	
}
?>