/**********
	CASES COURS
	**********/
	$i =0;
	foreach($edt as $ligne){
		
		if($_POST[$i . '_afficher'] == 'on' || $ligne['afficher'] == 1){
			
			$largeur = $l_caseX + 2;
			if($ligne['frequence'] != 1)
				$largeur = $largeur / 2 - 2.6;
			$largeur = 'width=' . $largeur . 'px; ';
			
			$duree_heure = $ligne['duree_h'];
			$duree_minute = $ligne['duree_m'];
			
			$hauteur = ($h_caseX + 2 ) * ($duree_heure + $duree_minute / 60) - 1 + ($duree_heure - 1 + $duree_minute / 60) * 4;
			$hauteur = 'height=' . $hauteur . 'px; ';
			
			$decalage_droite = 36.1 + ($ligne['n_jour'])*($l_caseX + 7.2);
			if($grille == 0)
				$decalage_droite = 36.1 + ($ligne['n_jour'])*($l_caseX + 7.1);
			
			if($ligne['semaine'] == 2)
				$decalage_droite = $decalage_droite + ($l_caseX + 6.9)/2;
			$decalage_droite = 'position : absolute; left:' . $decalage_droite . 'px; ';
			
			$decalage_bas = 71 + ($ligne['h_deb'] - $min)*($h_caseX + 6.07) + ($ligne['m_deb']/60)*($h_caseX + 6);
			$decalage_bas = 'top:' . $decalage_bas . 'px; ';
			
			//couleur
			$uv = $ligne['uv']; //récupération de l'UV en cours
			$r = $liste_uv[$uv]['r'];
			$g = $liste_uv[$uv]['g'];
			$b = $liste_uv[$uv]['b'];
			
			$couleur = 'background-color: rgb('. $r .','. $g .',' . $b .'); color:' . $liste_uv[$uv]['txt'] . ';';
			
			$chaine_style = $largeur . $hauteur . $decalage_droite . $decalage_bas . $couleur;
			
			if($ligne['type'] == 'C')
				$ligne['type'] = 'Cours';
			if($ligne['type'] == 'T')
				$ligne['type'] = 'TP';
			if($ligne['type'] == 'D')
				$ligne['type'] = 'TD';
			if($ligne['type'] == 'E')
				$ligne['type'] = 'E';
				
			if($ligne['m_deb'] == 0)
				$ligne['m_deb'] = '00';
			if($ligne['m_fin'] == 0)
				$ligne['m_fin'] = '00';
				
			echo '<div class="case_cours" style="'. $chaine_style .'"><strong>'. $ligne['uv'] . '<br/></strong>';
			if($ligne['salle'] != '0')
				echo '<em>' . $ligne['type'] . ' en ' . $ligne['salle'] .'<br/>';
			else
				echo '<em>';
			echo $ligne['h_deb'] . 'h' . $ligne['m_deb'] . ' - ' . $ligne['h_fin'] . 'h' . $ligne['m_fin'] . '</em></div>';
	
		}
		$i++;
	}
	
	/******
	LEGENDE
	******/
	if($a5 != 1){
		//Compte uvs
		$nb_uv = 0;
		$i = 0;
		foreach($liste_uv as $ligne){
			$nb_uv++;
			$nom_uv[$i] = $ligne['uv'];
			$i++;
		}
		
		//nombre de colonnes (duos de colonnes) et leur largeur
		
		$nb_col = floor(($nb_uv - 0.1) / 3) + 1;
		$petite_larg = 9; // en mm
		$grande_larg = 297/$nb_col - 8; // en mm
		
		//contenu de la légende
		
		include('bdd.php');
		echo '<table class="legende"><tr>';
		
		$uv_traitees_A = 0;
		$uv_traitees_B = 0;
		for($j = 0; $j < $nb_col; $j++){
			echo '<td style="width: '. $petite_larg .'mm;">';
			
			for($i = 0; $i < 3; $i ++){
				
				$retour_uv = mysql_query("SELECT * FROM uv WHERE uv='" . $nom_uv[3 * $j + $i] . "'");
				$tableau_legende = mysql_fetch_array($retour_uv);
				echo $tableau_legende['uv'] . '<br/>';
				
				$uv_traitees_A++;
				
				if($uv_traitees_A == $nb_uv)
					$i = 3;
			}
			
			echo '</td><td style="width: '. $grande_larg .'mm;">';
			
			for($i = 0; $i < 3; $i ++){
				
				$retour_uv = mysql_query("SELECT * FROM uv WHERE uv='" . $nom_uv[3 * $j + $i] . "'");
				$tableau_legende = mysql_fetch_array($retour_uv);
				echo $tableau_legende['legende'] . '<br/>';
				
				$uv_traitees_B++;
				
				if($uv_traitees_B == $nb_uv)
					$i = 3;
			}
			
			echo '</td>';
		}
		
		echo '</tr></table>';
		
		mysql_close();
	}
	if($a5 == 1)
		echo '<div style="border-bottom:1px dashed rgb(128,128,128); position:absolute;top:-5mm;left:-5mm; width:210mm; height:148mm;"></div>';
?>