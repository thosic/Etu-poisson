<?php 
session_start();
include('bdd.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
	<head>
		<title>Connexion</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" media="screen" type="text/css" title="Design" href="design.css" />
		<style type="text/css">
			textarea{
				display:block;
			}
					</style>
	</head>
	<body>




<div id="corps">
<h1><a href="index.php">Générateur d'emplois du temps utc <span class="petit">(en attendant de trouver un autre nom…)</span></a></h1>
<h2>Suppression du compte</h2>
<?php
if(!isset($_POST['supprimer'])){
?>


Cette page te permet de supprimer ton compte : tes identifiants seront alors immédiatement supprimés de la base de donnée, ainsi que ton emploi du temps.<br/><br/>
<form method="post" action="supprimer_compte.php">
	<input type="hidden" name="supprimer" value="1"/>
	<input type="submit" value="Supprimer mon compte"/>
</form>
<?php
}
elseif(isset($_POST['supprimer'])){
	
	mysql_query("DELETE FROM compte WHERE email='". $_SESSION['membre_id'] ."'");
	mysql_query("DELETE FROM graphique WHERE login='". $_SESSION['membre_id'] ."'");
	mysql_query("DELETE FROM cours WHERE email='". $_SESSION['membre_id'] ."'");

	session_destroy();

	echo "Ton compte a été supprimé. <br/>Redirection vers la page d'accueil dans 2 secondes.";
	echo '<meta http-equiv="Refresh" content="2;URL=index.php">';

}
?>

</div>
	</body>
</html>

<?php
mysql_close();
?>